USE [shoes]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](60) NULL,
	[Password] [varchar](15) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[Name] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill_Detail]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Bill] [int] NULL,
	[ID_Product_Color] [int] NULL,
	[Quantity] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bills]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bills](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Customer] [int] NULL,
	[Date_order] [datetime] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[Address] [nvarchar](100) NULL,
	[Phone_Number] [int] NULL,
	[Confirm] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Brand]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brand](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[Description] [nvarchar](300) NULL,
	[Image] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Product_Color] [int] NULL,
	[ID_Customer] [int] NULL,
	[Quantity_Purchased] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Colorr]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Colorr](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Color] [nvarchar](20) NULL,
	[Image] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Password] [varchar](15) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Gender] [nvarchar](20) NULL,
	[Address] [nvarchar](200) NULL,
	[Phone_Number] [int] NULL,
	[note] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[Status] [char](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Favorites_list]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Favorites_list](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Product] [int] NULL,
	[ID_Customer] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Imagee]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Imagee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Product] [int] NULL,
	[Image] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[New_Images]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[New_Images](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_New] [int] NULL,
	[Image] [nvarchar](300) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[Content] [ntext] NULL,
	[Image] [nvarchar](200) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ID_Brand] [int] NULL,
	[Description] [nvarchar](300) NULL,
	[Price] [money] NULL,
	[Promotion_Price] [money] NULL,
	[Image] [nvarchar](200) NULL,
	[Warranty] [nvarchar](50) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Color]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Color](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Color] [int] NULL,
	[ID_Product] [int] NULL,
	[Quantity] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[size] [char](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Slide]    Script Date: 4/13/2021 2:07:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slide](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](100) NULL,
	[Title] [nvarchar](100) NULL,
	[Content] [nvarchar](500) NULL,
	[Action] [nvarchar](30) NULL,
	[Discount] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([ID], [Email], [Password], [created_at], [updated_at], [Name]) VALUES (1, N'zaynmilak99@gmail.com', N'123', NULL, NULL, N'Zayn')
SET IDENTITY_INSERT [dbo].[Admin] OFF
GO
SET IDENTITY_INSERT [dbo].[Bill_Detail] ON 

INSERT [dbo].[Bill_Detail] ([ID], [ID_Bill], [ID_Product_Color], [Quantity], [created_at], [updated_at]) VALUES (1, 1, 68, 2, NULL, NULL)
INSERT [dbo].[Bill_Detail] ([ID], [ID_Bill], [ID_Product_Color], [Quantity], [created_at], [updated_at]) VALUES (2, 2, 206, 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Bill_Detail] OFF
GO
SET IDENTITY_INSERT [dbo].[Bills] ON 

INSERT [dbo].[Bills] ([ID], [ID_Customer], [Date_order], [created_at], [updated_at], [Address], [Phone_Number], [Confirm]) VALUES (1, 1, CAST(N'2021-04-05T08:20:21.543' AS DateTime), NULL, NULL, N'Nhà số 9, ngách 99/48 Phường Phúc Diễn Quận Bắc Từ Liêm Hà Nội', 852049203, N'Chờ xác nhận')
INSERT [dbo].[Bills] ([ID], [ID_Customer], [Date_order], [created_at], [updated_at], [Address], [Phone_Number], [Confirm]) VALUES (2, 1, CAST(N'2021-04-12T08:48:54.593' AS DateTime), NULL, NULL, N'Nhà số 9, ngách 99/48 Phường Phúc Diễn Quận Bắc Từ Liêm Hà Nội', 852049203, N'Chờ xác nhận')
SET IDENTITY_INSERT [dbo].[Bills] OFF
GO
SET IDENTITY_INSERT [dbo].[Brand] ON 

INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (1, N'NIKE', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/9-5404bf6a-5b82-4dd4-bb96-40fe73cc8ad0.jpg?v=1616478847267', CAST(N'2021-04-01T08:42:00.840' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (2, N'ADIDAS', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/50003341-2365870303644467-7038016969261449216-o.jpg?v=1547535181000', CAST(N'2021-04-01T08:43:19.477' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (3, N'BALENCIAGA', N'Chất lượng Rep 1:1 ', N'https://giaygiare.vn/upload/sanpham/thumbs/balenciaga-track-triple-white-nam-nu-1-1.jpg', CAST(N'2021-04-01T08:47:47.027' AS DateTime), CAST(N'2021-04-01T08:48:57.130' AS DateTime))
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (4, N'GUCCI', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/dscf2988.jpg?v=1563162475013', CAST(N'2021-04-01T08:49:42.263' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (5, N'VANS', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/88-40092ec2-9c6a-4867-8a70-ba056fd33d8f.jpg?v=1547967316000', CAST(N'2021-04-01T08:50:20.350' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (6, N'CONVERSE', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/17.jpg?v=1546919922130', CAST(N'2021-04-01T08:51:07.650' AS DateTime), NULL)
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (7, N'MLB', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/dscf4560.jpg?v=1573540617000', CAST(N'2021-04-01T09:42:51.780' AS DateTime), CAST(N'2021-04-01T09:44:46.960' AS DateTime))
INSERT [dbo].[Brand] ([ID], [Name], [Description], [Image], [created_at], [updated_at]) VALUES (10, N'NEW BALANCE', N'Chất lượng Rep 1:1 ', N'https://bizweb.dktcdn.net/100/336/177/products/dscf6661.jpg?v=1592988269917', CAST(N'2021-04-01T09:43:45.770' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Brand] OFF
GO
SET IDENTITY_INSERT [dbo].[Colorr] ON 

INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (1, N'Đen', N'https://bizweb.dktcdn.net/100/336/177/products/10-81ed6bc2-ac8a-4b0a-8746-7ca4de21a0ab.jpg?v=1616478878337', CAST(N'2021-04-02T07:48:06.530' AS DateTime), CAST(N'2021-04-02T07:48:24.220' AS DateTime))
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (2, N'Trắng', N'https://bizweb.dktcdn.net/100/336/177/products/16-7101df84-9f82-4179-830d-67993c5ca4a1.jpg?v=1616478946000', CAST(N'2021-04-02T07:50:01.697' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (3, N'Đỏ', N'https://bizweb.dktcdn.net/100/336/177/products/27-3e2e1f85-449a-46f8-85ea-72ddd361145e.jpg?v=1611559414000', CAST(N'2021-04-02T07:50:17.477' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (4, N'Vàng', N'https://bizweb.dktcdn.net/100/336/177/products/dscf6243.jpg?v=1590302139563', CAST(N'2021-04-02T07:50:41.070' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (5, N'Xanh', N'https://bizweb.dktcdn.net/100/336/177/products/64.jpg?v=1606282824817', CAST(N'2021-04-02T07:51:13.843' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (6, N'Xám', N'https://bizweb.dktcdn.net/100/336/177/products/60.jpg?v=1606282851000', CAST(N'2021-04-02T07:51:38.303' AS DateTime), NULL)
INSERT [dbo].[Colorr] ([ID], [Color], [Image], [created_at], [updated_at]) VALUES (7, N'Cam', N'https://bizweb.dktcdn.net/100/336/177/products/2-1.jpg?v=1598953701000', CAST(N'2021-04-02T07:52:07.137' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Colorr] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([ID], [Email], [Password], [Name], [Gender], [Address], [Phone_Number], [note], [created_at], [updated_at], [Status]) VALUES (1, N'zaynmilak99@gmail.com', N'1234', N'Thắng Nguyễn', N'Nam', N'Nhà số 9, ngách 99/48 Phường Phúc Diễn Quận Bắc Từ Liêm Hà Nội', 852049203, NULL, CAST(N'2021-04-05T08:19:26.780' AS DateTime), CAST(N'2021-04-05T16:31:09.603' AS DateTime), N'Active              ')
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[Favorites_list] ON 

INSERT [dbo].[Favorites_list] ([ID], [ID_Product], [ID_Customer], [created_at], [updated_at]) VALUES (5, 1, 1, CAST(N'2021-04-08T07:40:52.120' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Favorites_list] OFF
GO
SET IDENTITY_INSERT [dbo].[Imagee] ON 

INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (7, 1, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/6-dad445b8-9d65-4381-a564-02429370df11.jpg?v=1616832056713', CAST(N'2021-04-02T08:25:39.747' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (8, 1, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/7-38850fad-629e-426f-a2ac-b6c7a607542c.jpg?v=1616832057260', CAST(N'2021-04-02T08:25:50.600' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (9, 1, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/8-14ace31a-a33f-4f3d-ac03-53ed008fee4e.jpg?v=1616832057857', CAST(N'2021-04-02T08:25:57.927' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (10, 1, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/9-85b97316-abee-4199-b47a-0b6ef247d45f.jpg?v=1616832058190', CAST(N'2021-04-02T08:26:04.863' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (11, 8, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-e9ca1c74-a64a-4822-aa6d-063d01888bb1.jpg?v=1616479119313', CAST(N'2021-04-05T11:08:05.637' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (12, 8, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-e9ca1c74-a64a-4822-aa6d-063d01888bb1.jpg?v=1616479119313', CAST(N'2021-04-05T11:08:12.630' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (13, 8, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/3-86e378e2-782f-4394-8362-c99b52502c69.jpg?v=1616479120120', CAST(N'2021-04-05T11:08:19.620' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (14, 8, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/4-511ef46f-1817-443e-b5e9-8cb573f37651.jpg?v=1616479120450', CAST(N'2021-04-05T11:08:28.280' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (15, 10, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/11-4ffeedd5-50b1-40aa-a2ae-7bdc259bb38d.jpg?v=1616478916343', CAST(N'2021-04-05T11:09:37.697' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (16, 10, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/12-b486e5d2-7d09-4947-838e-62d428bb6716.jpg?v=1616478916603', CAST(N'2021-04-05T11:09:45.710' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (17, 10, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/13-7aedd029-69af-4c5d-aadc-cd2a2c047f5b.jpg?v=1616478917127', CAST(N'2021-04-05T11:09:53.270' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (18, 10, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/14-d5ba0472-3c9f-4017-8ce8-7ffa8cda3128.jpg?v=1616478917437', CAST(N'2021-04-05T11:10:01.960' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (19, 9, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/23-4a4313ba-8550-4b43-9a53-7721960690ba.jpg?v=1616479015763', CAST(N'2021-04-05T11:10:54.797' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (20, 9, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/23-4a4313ba-8550-4b43-9a53-7721960690ba.jpg?v=1616479015763', CAST(N'2021-04-05T11:10:59.137' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (21, 9, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/25-7e2f9589-4800-4fe6-a3a4-d07bbbddf023.jpg?v=1616479016613', CAST(N'2021-04-05T11:11:05.570' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (22, 9, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/26-142d6f9c-082d-403e-a7b9-eaa9a25ddc9b.jpg?v=1616479017093', CAST(N'2021-04-05T11:11:12.543' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (23, 11, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/26-638e34cd-4d70-44a9-a1c5-de52e2310a94.jpg?v=1611559414207', CAST(N'2021-04-05T11:11:59.327' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (24, 11, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/26-638e34cd-4d70-44a9-a1c5-de52e2310a94.jpg?v=1611559414207', CAST(N'2021-04-05T11:12:02.643' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (25, 11, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/28-ce305c50-ca15-468a-a27e-a44ac4547da5.jpg?v=1611559415193', CAST(N'2021-04-05T11:12:09.450' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (26, 11, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/29-e7e9c4df-6430-4777-b47d-cdda018abecf.jpg?v=1611559415500', CAST(N'2021-04-05T11:12:17.717' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (27, 14, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/22-e3c92372-829c-4377-9991-d5c752e8eef1.jpg?v=1611559593950', CAST(N'2021-04-05T13:25:01.290' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (28, 14, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/22-e3c92372-829c-4377-9991-d5c752e8eef1.jpg?v=1611559593950', CAST(N'2021-04-05T13:25:07.840' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (29, 14, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/24-44109e3d-b456-413c-a8d3-abf40e6ec56c.jpg?v=1611559594790', CAST(N'2021-04-05T13:25:15.143' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (30, 14, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/25-439a1b8b-26d2-4d97-9d18-ed69a3bf9381.jpg?v=1611559595270', CAST(N'2021-04-05T13:25:23.520' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (31, 15, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-4bea8a57-bced-4f79-9a6b-1e436ffdf979.jpg?v=1610525298593', CAST(N'2021-04-05T13:25:57.673' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (32, 15, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-4bea8a57-bced-4f79-9a6b-1e436ffdf979.jpg?v=1610525298593', CAST(N'2021-04-05T13:26:01.807' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (33, 15, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/3-66920506-bc0d-4f1a-8812-d31a733bbd80.jpg?v=1610525299563', CAST(N'2021-04-05T13:26:08.407' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (34, 15, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/4-ff132ae1-1958-462a-8894-78f092cdd2ea.jpg?v=1610525299830', CAST(N'2021-04-05T13:26:14.813' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (35, 17, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/67.jpg?v=1606282794180', CAST(N'2021-04-05T13:26:49.577' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (36, 17, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/68.jpg?v=1606282794633', CAST(N'2021-04-05T13:26:58.257' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (37, 17, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/69.jpg?v=1606282794887', CAST(N'2021-04-05T13:27:09.007' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (38, 17, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/70.jpg?v=1606282795127', CAST(N'2021-04-05T13:27:15.580' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (39, 18, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/31.jpg?v=1605765076400', CAST(N'2021-04-05T13:27:36.363' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (40, 18, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/32.jpg?v=1605765077423', CAST(N'2021-04-05T13:27:43.953' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (41, 18, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/33.jpg?v=1605765078297', CAST(N'2021-04-05T13:27:50.533' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (42, 18, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/34-36ad6bf9-8ef9-4220-bb20-83d30204a7f8.jpg?v=1605765079143', CAST(N'2021-04-05T13:27:57.213' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (43, 19, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6599.jpg?v=1592394663710', CAST(N'2021-04-05T13:28:37.253' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (44, 19, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6600.jpg?v=1592394665917', CAST(N'2021-04-05T13:28:47.290' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (45, 19, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6601.jpg?v=1592394668177', CAST(N'2021-04-05T13:28:53.943' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (46, 19, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6602.jpg?v=1592394670390', CAST(N'2021-04-05T13:29:02.673' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (47, 20, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2920.jpg?v=1562751354087', CAST(N'2021-04-05T13:29:19.893' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (48, 20, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2921.jpg?v=1562751355207', CAST(N'2021-04-05T13:29:29.080' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (49, 20, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2922.jpg?v=1562751356133', CAST(N'2021-04-05T13:29:35.540' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (50, 20, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2924.jpg?v=1562751357437', CAST(N'2021-04-05T13:29:42.773' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (51, 21, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/11-9d92fa80-dc2c-4ebd-a61f-378c03d8aa4b.jpg?v=1546919912917', CAST(N'2021-04-05T13:33:28.903' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (52, 21, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/12-80466ec9-118e-4db8-8596-8ac5c12c09c2.jpg?v=1546919912917', CAST(N'2021-04-05T13:33:35.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (53, 21, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/13-132303fd-897b-4df3-9482-62b7f7e294bd.jpg?v=1546919912917', CAST(N'2021-04-05T13:33:42.287' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (54, 21, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/14-c8679944-8361-4837-b41a-cd3cc8c2018e.jpg?v=1546919912917', CAST(N'2021-04-05T13:33:49.210' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (55, 22, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/6.jpg?v=1586864907913', CAST(N'2021-04-05T13:34:04.213' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (56, 22, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/6.jpg?v=1586864907913', CAST(N'2021-04-05T13:34:07.250' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (57, 22, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/8.jpg?v=1586864907913', CAST(N'2021-04-05T13:34:14.590' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (58, 22, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/9.jpg?v=1586864907913', CAST(N'2021-04-05T13:34:21.623' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (59, 23, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-63cca0a8-76b0-4d2e-9d5c-e07322672796.jpg?v=1611560048407', CAST(N'2021-04-05T13:34:39.147' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (60, 23, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/2-831fcf72-a027-4602-b7a7-072af82c2dd4.jpg?v=1611560048870', CAST(N'2021-04-05T13:34:56.093' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (61, 23, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/3-e2f515e0-7946-4e80-98ed-6e0ff7a58124.jpg?v=1611560049373', CAST(N'2021-04-05T13:35:03.577' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (62, 23, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/4-fc004c34-f5cb-4be5-8cfc-65a1d8ffcec8.jpg?v=1611560049840', CAST(N'2021-04-05T13:35:10.057' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (63, 25, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-1-615a8554-2631-42ff-a271-d3fba83a9240.jpg?v=1610525331493', CAST(N'2021-04-05T13:35:41.937' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (64, 25, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/2-1-5d469925-98d1-4cb7-82e8-51e7dbdad278.jpg?v=1610525332037', CAST(N'2021-04-05T13:35:49.813' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (65, 25, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/7-3355edfb-21d2-49c9-94dd-a1ecd32e14c5.jpg?v=1610525332373', CAST(N'2021-04-05T13:35:57.420' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (66, 25, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/8-fcd6899f-496e-49bb-ac54-047d06af7edf.jpg?v=1610525332670', CAST(N'2021-04-05T13:36:04.920' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (67, 26, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/1-825a5df8-3992-40e4-9a4c-69889146246c.jpg?v=1602405504007', CAST(N'2021-04-05T13:36:28.847' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (68, 26, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/2-48d0d16d-e578-47ce-85cc-a7be727ee021.jpg?v=1602405505600', CAST(N'2021-04-05T13:36:36.733' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (69, 26, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/3-7653037a-dcec-4365-b989-9c66e1b3d2ce.jpg?v=1602405506433', CAST(N'2021-04-05T13:36:45.990' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (70, 26, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/4-78a09c8f-6da9-4353-967b-eebacdc1a7d2.jpg?v=1602405507273', CAST(N'2021-04-05T13:36:53.233' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (71, 27, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9711.jpg?v=1604392718100', CAST(N'2021-04-05T13:37:13.243' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (72, 27, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9712.jpg?v=1604392720600', CAST(N'2021-04-05T13:37:20.843' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (73, 27, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9713.jpg?v=1604392723253', CAST(N'2021-04-05T13:37:27.243' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (74, 27, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9714.jpg?v=1604392726013', CAST(N'2021-04-05T13:37:34.067' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (75, 28, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6693.jpg?v=1592988685157', CAST(N'2021-04-05T13:39:22.513' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (76, 28, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6694.jpg?v=1592988687157', CAST(N'2021-04-05T13:39:29.720' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (77, 28, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6695.jpg?v=1592988689030', CAST(N'2021-04-05T13:39:36.797' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (78, 28, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6696.jpg?v=1592988690763', CAST(N'2021-04-05T13:39:43.340' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (79, 29, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6605.jpg?v=1592394850703', CAST(N'2021-04-05T13:39:59.307' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (80, 29, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6606.jpg?v=1592394853213', CAST(N'2021-04-05T13:40:08.053' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (81, 29, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6607.jpg?v=1592394855880', CAST(N'2021-04-05T13:40:15.470' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (82, 29, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6609.jpg?v=1592394858193', CAST(N'2021-04-05T13:40:21.393' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (83, 30, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/15-48373202-78de-482e-bbe9-d0aae6ca215d.jpg?v=1586864978403', CAST(N'2021-04-05T13:40:43.660' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (84, 30, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/16-6f1033e3-1838-40d7-9e86-04a3c0038498.jpg?v=1586864978850', CAST(N'2021-04-05T13:40:52.273' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (85, 30, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/17-9aa6c862-a847-4dc0-b3df-41f152854b37.jpg?v=1586864979467', CAST(N'2021-04-05T13:40:59.443' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (86, 31, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3579.jpg?v=1565424144283', CAST(N'2021-04-05T13:41:14.823' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (87, 31, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3580.jpg?v=1565424145477', CAST(N'2021-04-05T13:41:22.883' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (88, 31, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3581.jpg?v=1565424146603', CAST(N'2021-04-05T13:41:31.433' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (89, 31, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3582.jpg?v=1565424147820', CAST(N'2021-04-05T13:41:38.077' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (90, 32, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3095.jpg?v=1562840806277', CAST(N'2021-04-05T13:41:55.360' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (91, 32, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3096.jpg?v=1562840806277', CAST(N'2021-04-05T13:42:04.933' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (92, 32, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3097.jpg?v=1562840806277', CAST(N'2021-04-05T13:42:11.953' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (93, 32, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3098.jpg?v=1562840806277', CAST(N'2021-04-05T13:42:18.043' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (94, 33, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/86.jpg?v=1547837362710', CAST(N'2021-04-05T13:42:31.513' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (95, 33, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/87.jpg?v=1547837364243', CAST(N'2021-04-05T13:42:41.990' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (96, 33, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/88.jpg?v=1547837365227', CAST(N'2021-04-05T13:42:49.907' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (97, 33, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/89.jpg?v=1547837366060', CAST(N'2021-04-05T13:42:57.863' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (98, 34, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/800502-01-1.jpg?v=1546919919267', CAST(N'2021-04-05T13:44:02.333' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (99, 34, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/800502-02.jpg?v=1546919919267', CAST(N'2021-04-05T13:44:10.177' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (100, 34, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/800502-03.jpg?v=1546919919267', CAST(N'2021-04-05T13:44:15.980' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (101, 34, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/800502-04.jpg?v=1546919919267', CAST(N'2021-04-05T13:44:23.257' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (102, 35, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/49422709-2365870980311066-7436626140904554496-o.jpg?v=1547535181723', CAST(N'2021-04-05T13:44:57.653' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (103, 35, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/50003341-2365870303644467-7038016969261449216-o.jpg?v=1547535181723', CAST(N'2021-04-05T13:45:03.873' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (104, 35, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/50104204-2365870356977795-2438374537348251648-o.jpg?v=1547535181723', CAST(N'2021-04-05T13:45:10.427' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (105, 35, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/49949082-2365870383644459-5185166226636144640-o.jpg?v=1547535181723', CAST(N'2021-04-05T13:45:16.847' AS DateTime), NULL)
GO
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (106, 36, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/21-966fb1dd-a0c4-4d19-8016-31d3c13375cf.jpg?v=1547361689080', CAST(N'2021-04-05T13:45:36.297' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (107, 36, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/22-0794bee4-5e0e-4a3e-a27f-89e14503e197.jpg?v=1547361689080', CAST(N'2021-04-05T13:45:44.100' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (108, 36, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/23-eb3265dd-2279-4dd0-bfcd-741fac45eced.jpg?v=1547361689080', CAST(N'2021-04-05T13:45:53.143' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (109, 36, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/24-b4eb706a-ebd4-4296-84ee-0c92b27b35c0.jpg?v=1547361689080', CAST(N'2021-04-05T13:45:59.463' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (110, 48, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6721.jpg?v=1592988113943', CAST(N'2021-04-05T13:46:42.157' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (111, 48, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6722.jpg?v=1592988116000', CAST(N'2021-04-05T13:46:51.423' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (112, 48, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6723.jpg?v=1592988117587', CAST(N'2021-04-05T13:46:59.413' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (113, 48, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6724.jpg?v=1592988119493', CAST(N'2021-04-05T13:47:10.053' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (114, 49, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2974.jpg?v=1562751235627', CAST(N'2021-04-05T13:47:25.460' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (115, 49, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2975.jpg?v=1562751236723', CAST(N'2021-04-05T13:47:32.740' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (116, 49, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2976.jpg?v=1562751237783', CAST(N'2021-04-05T13:47:40.423' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (117, 49, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2977.jpg?v=1562751238720', CAST(N'2021-04-05T13:47:48.003' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (118, 50, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2963.jpg?v=1562751312203', CAST(N'2021-04-05T13:48:06.697' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (119, 50, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2964.jpg?v=1562751313383', CAST(N'2021-04-05T13:48:15.067' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (120, 50, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2965.jpg?v=1562751314213', CAST(N'2021-04-05T13:48:24.797' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (121, 50, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2966.jpg?v=1562751315057', CAST(N'2021-04-05T13:48:34.107' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (122, 51, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2986.jpg?v=1563162475013', CAST(N'2021-04-05T13:48:53.550' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (123, 51, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2988.jpg?v=1563162475013', CAST(N'2021-04-05T13:49:02.547' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (124, 51, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2989.jpg?v=1563162475013', CAST(N'2021-04-05T13:49:14.170' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (125, 51, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2990.jpg?v=1563162475013', CAST(N'2021-04-05T13:49:22.433' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (126, 52, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2980.jpg?v=1563162535740', CAST(N'2021-04-05T13:50:25.650' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (127, 52, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2981.jpg?v=1563162535740', CAST(N'2021-04-05T13:50:39.320' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (128, 52, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2982.jpg?v=1563162535740', CAST(N'2021-04-05T13:50:46.633' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (129, 52, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf2983.jpg?v=1563162535740', CAST(N'2021-04-05T13:50:54.580' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (130, 53, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4670.jpg?v=1575438868097', CAST(N'2021-04-05T13:51:15.183' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (131, 53, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4671.jpg?v=1575438868097', CAST(N'2021-04-05T13:51:22.743' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (132, 53, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4672.jpg?v=1575438868097', CAST(N'2021-04-05T13:51:28.837' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (133, 53, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4673.jpg?v=1575438868097', CAST(N'2021-04-05T13:51:36.517' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (134, 54, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9056.jpg?v=1598952561870', CAST(N'2021-04-05T13:52:09.547' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (135, 54, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9057.jpg?v=1598952563603', CAST(N'2021-04-05T13:52:21.233' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (136, 54, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9058.jpg?v=1598952565263', CAST(N'2021-04-05T13:52:29.643' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (137, 54, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9059.jpg?v=1598952567220', CAST(N'2021-04-05T13:52:43.753' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (138, 55, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9367.jpg?v=1602243560420', CAST(N'2021-04-05T13:53:08.920' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (139, 55, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9368.jpg?v=1602243562447', CAST(N'2021-04-05T13:53:30.363' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (140, 55, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9369.jpg?v=1602243564577', CAST(N'2021-04-05T13:53:39.753' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (141, 55, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9370.jpg?v=1602243566353', CAST(N'2021-04-05T13:53:47.470' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (142, 56, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3849.jpg?v=1570871051217', CAST(N'2021-04-05T13:54:02.913' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (143, 56, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3850.jpg?v=1570871052020', CAST(N'2021-04-05T13:54:09.353' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (144, 56, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3851.jpg?v=1570871052963', CAST(N'2021-04-05T13:54:16.920' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (145, 56, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3852.jpg?v=1570871053750', CAST(N'2021-04-05T13:54:24.057' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (146, 57, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3844.jpg?v=1570870921270', CAST(N'2021-04-05T13:54:44.203' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (147, 57, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3845.jpg?v=1570870922023', CAST(N'2021-04-05T13:54:52.137' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (148, 57, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3846.jpg?v=1570870924397', CAST(N'2021-04-05T13:54:59.240' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (149, 57, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3847.jpg?v=1570870925697', CAST(N'2021-04-05T13:55:06.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (150, 58, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/113.jpg?v=1547967272397', CAST(N'2021-04-05T13:56:16.917' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (151, 58, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/114.jpg?v=1547967272397', CAST(N'2021-04-05T13:56:23.653' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (152, 58, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/115.jpg?v=1547967272397', CAST(N'2021-04-05T13:56:31.327' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (153, 58, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/116.jpg?v=1547967272397', CAST(N'2021-04-05T13:56:37.410' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (154, 59, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/102.jpg?v=1547967343350', CAST(N'2021-04-05T13:57:17.113' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (155, 59, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/103.jpg?v=1547967343350', CAST(N'2021-04-05T13:57:23.930' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (156, 59, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/104.jpg?v=1547967343350', CAST(N'2021-04-05T13:57:31.587' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (157, 59, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/105.jpg?v=1547967350050', CAST(N'2021-04-05T13:57:40.323' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (158, 62, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/87-f236e24b-fde9-405e-91a3-4dcdb0429374.jpg?v=1547967316320', CAST(N'2021-04-05T13:58:00.217' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (159, 62, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/88-40092ec2-9c6a-4867-8a70-ba056fd33d8f.jpg?v=1547967316993', CAST(N'2021-04-05T13:58:07.387' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (160, 62, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/89-43ebba87-b6d5-4269-8e4e-b09029b4950c.jpg?v=1547967317707', CAST(N'2021-04-05T13:58:17.063' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (161, 62, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/90-2b2dbbba-bcce-48ec-83b1-038112f7fcaa.jpg?v=1547967318317', CAST(N'2021-04-05T13:58:23.810' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (162, 63, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3650-cfd0ba49-d9cb-4743-a36f-a930cb93c658.jpg?v=1566628717663', CAST(N'2021-04-05T13:58:46.913' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (163, 63, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3651-79a63de2-99a6-423b-80f2-b8634848f4f2.jpg?v=1566628717663', CAST(N'2021-04-05T13:58:54.087' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (164, 63, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3652-38613c68-d6e0-4529-a2ae-2711100181ec.jpg?v=1566628717663', CAST(N'2021-04-05T13:58:59.630' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (165, 63, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf3653-ca514aa4-06b2-45fc-a2b9-48740a74e86e.jpg?v=1566628717663', CAST(N'2021-04-05T13:59:06.013' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (166, 64, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6678.jpg?v=1592988531653', CAST(N'2021-04-05T13:59:18.313' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (167, 64, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6679.jpg?v=1592988533490', CAST(N'2021-04-05T13:59:24.683' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (168, 64, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6680.jpg?v=1592988535233', CAST(N'2021-04-05T13:59:31.730' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (169, 64, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6681.jpg?v=1592988537020', CAST(N'2021-04-05T13:59:38.337' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (170, 65, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46353009-2327007670864064-2118245993139929088-o.jpg?v=1546919915250', CAST(N'2021-04-05T13:59:56.750' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (171, 65, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46425094-2327006747530823-1323242581668134912-o.jpg?v=1546919915250', CAST(N'2021-04-05T14:00:03.153' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (172, 65, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46413071-2327006360864195-7519902069260550144-o.jpg?v=1546919915250', CAST(N'2021-04-05T14:00:10.347' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (173, 65, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46486211-2327007944197370-7576389539267084288-o.jpg?v=1546919915250', CAST(N'2021-04-05T14:00:17.187' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (174, 66, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46482973-2327008257530672-8317736942217199616-o.jpg?v=1546919915567', CAST(N'2021-04-05T14:00:33.800' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (175, 66, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46420111-2327007577530740-6101184310222520320-o.jpg?v=1546919915567', CAST(N'2021-04-05T14:00:41.417' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (176, 66, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46434980-2327007207530777-3781173758677483520-o.jpg?v=1546919915567', CAST(N'2021-04-05T14:00:46.890' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (177, 66, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/46377539-2327008284197336-1270197162877124608-o.jpg?v=1546919915567', CAST(N'2021-04-05T14:00:53.073' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (178, 67, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/16.jpg?v=1546919922130', CAST(N'2021-04-05T14:02:27.623' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (179, 67, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/17.jpg?v=1546919922130', CAST(N'2021-04-05T14:02:34.637' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (180, 67, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/18.jpg?v=1546919922130', CAST(N'2021-04-05T14:02:39.573' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (181, 67, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/19.jpg?v=1546919922130', CAST(N'2021-04-05T14:02:46.020' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (182, 68, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/11.jpg?v=1546919922233', CAST(N'2021-04-05T14:02:57.133' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (183, 68, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/12.jpg?v=1546919922233', CAST(N'2021-04-05T14:03:02.690' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (184, 68, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/13.jpg?v=1546919922233', CAST(N'2021-04-05T14:03:09.280' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (185, 68, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/14.jpg?v=1546919922233', CAST(N'2021-04-05T14:03:16.007' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (186, 69, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/42663248-2297178860513612-7237781713584128000-o-2297178853846946.jpg?v=1546919922333', CAST(N'2021-04-05T14:03:30.980' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (187, 69, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/42669430-2297178037180361-6679355885438894080-o-2297178033847028.jpg?v=1546919922333', CAST(N'2021-04-05T14:03:38.927' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (188, 69, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/42719833-2297178053847026-358641326972993536-o-2297178043847027.jpg?v=1546919922333', CAST(N'2021-04-05T14:03:44.363' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (189, 69, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/42646553-2297179367180228-1783361470310907904-o-2297179360513562.jpg?v=1546919922333', CAST(N'2021-04-05T14:03:50.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (190, 72, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9238.jpg?v=1611741894593', CAST(N'2021-04-05T14:04:29.673' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (191, 72, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9239.jpg?v=1611741896127', CAST(N'2021-04-05T14:04:37.153' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (192, 72, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9240.jpg?v=1611741897383', CAST(N'2021-04-05T14:04:45.353' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (193, 72, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9241.jpg?v=1611741898760', CAST(N'2021-04-05T14:04:51.410' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (194, 73, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4559.jpg?v=1573540616160', CAST(N'2021-04-05T14:05:05.577' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (195, 73, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4560.jpg?v=1573540617447', CAST(N'2021-04-05T14:05:12.927' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (196, 73, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4561.jpg?v=1573540618487', CAST(N'2021-04-05T14:05:18.260' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (197, 73, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4562.jpg?v=1573540619567', CAST(N'2021-04-05T14:05:23.930' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (198, 74, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4569.jpg?v=1573540759610', CAST(N'2021-04-05T14:05:38.167' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (199, 74, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4570.jpg?v=1573540760577', CAST(N'2021-04-05T14:05:49.690' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (200, 74, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4571.jpg?v=1573540761420', CAST(N'2021-04-05T14:05:53.000' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (201, 74, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4572.jpg?v=1573540762627', CAST(N'2021-04-05T14:06:04.070' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (202, 75, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6560.jpg?v=1592120818850', CAST(N'2021-04-05T14:06:15.770' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (203, 75, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6561.jpg?v=1592120820447', CAST(N'2021-04-05T14:06:23.257' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (204, 75, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6562.jpg?v=1592120821997', CAST(N'2021-04-05T14:06:30.073' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (205, 75, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6563.jpg?v=1592120823547', CAST(N'2021-04-05T14:06:35.860' AS DateTime), NULL)
GO
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (206, 76, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9688.jpg?v=1604392884637', CAST(N'2021-04-05T14:06:59.927' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (207, 76, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9689.jpg?v=1604392887707', CAST(N'2021-04-05T14:07:06.877' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (208, 76, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9690.jpg?v=1604392890763', CAST(N'2021-04-05T14:07:13.013' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (209, 76, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9691.jpg?v=1604392893493', CAST(N'2021-04-05T14:07:18.993' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (210, 77, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9694.jpg?v=1604393030803', CAST(N'2021-04-05T14:07:41.387' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (211, 77, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9695.jpg?v=1604393033690', CAST(N'2021-04-05T14:07:50.690' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (212, 77, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9696.jpg?v=1604393036357', CAST(N'2021-04-05T14:07:58.553' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (213, 77, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf9697.jpg?v=1604393039220', CAST(N'2021-04-05T14:08:04.643' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (214, 78, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4574.jpg?v=1573540720947', CAST(N'2021-04-05T14:08:16.580' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (215, 78, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4575.jpg?v=1573540721977', CAST(N'2021-04-05T14:08:22.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (216, 78, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4576.jpg?v=1573540723040', CAST(N'2021-04-05T14:08:28.207' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (217, 78, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4577.jpg?v=1573540724203', CAST(N'2021-04-05T14:08:34.123' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (218, 79, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4585.jpg?v=1573540651657', CAST(N'2021-04-05T14:08:50.000' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (219, 79, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4586.jpg?v=1573540652670', CAST(N'2021-04-05T14:10:15.637' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (220, 79, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4587.jpg?v=1573540653543', CAST(N'2021-04-05T14:10:22.177' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (221, 79, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf4588.jpg?v=1573540654773', CAST(N'2021-04-05T14:10:27.370' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (222, 80, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6655.jpg?v=1599816561337', CAST(N'2021-04-05T14:10:41.307' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (223, 80, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6656.jpg?v=1599816561337', CAST(N'2021-04-05T14:10:48.153' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (224, 80, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6657.jpg?v=1599816561337', CAST(N'2021-04-05T14:10:55.200' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (225, 80, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6658.jpg?v=1599816561337', CAST(N'2021-04-05T14:11:03.090' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (227, 81, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6673.jpg?v=1592988243687', CAST(N'2021-04-05T14:11:57.167' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (228, 81, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6674.jpg?v=1592988245783', CAST(N'2021-04-05T14:12:06.763' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (229, 81, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6675.jpg?v=1592988247387', CAST(N'2021-04-05T14:12:15.300' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (230, 81, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6676.jpg?v=1592988248967', CAST(N'2021-04-05T14:12:22.437' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (231, 82, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6660.jpg?v=1592988268027', CAST(N'2021-04-05T14:12:38.220' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (232, 82, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6661.jpg?v=1592988269917', CAST(N'2021-04-05T14:12:46.353' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (233, 82, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6663.jpg?v=1592988271543', CAST(N'2021-04-05T14:12:54.150' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (235, 82, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6664.jpg?v=1592988273147', CAST(N'2021-04-05T14:13:07.120' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (236, 83, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6668.jpg?v=1592988334557', CAST(N'2021-04-05T14:13:27.047' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (237, 83, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6669.jpg?v=1592988336397', CAST(N'2021-04-05T14:13:32.440' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (238, 83, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6670.jpg?v=1592988338190', CAST(N'2021-04-05T14:13:37.790' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (239, 83, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6671.jpg?v=1592988339977', CAST(N'2021-04-05T14:13:46.353' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (240, 84, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6649.jpg?v=1592988354877', CAST(N'2021-04-05T14:14:05.967' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (241, 84, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6650.jpg?v=1592988356500', CAST(N'2021-04-05T14:14:11.150' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (242, 84, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6651.jpg?v=1592988358067', CAST(N'2021-04-05T14:14:16.617' AS DateTime), NULL)
INSERT [dbo].[Imagee] ([ID], [ID_Product], [Image], [created_at], [updated_at]) VALUES (243, 84, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/dscf6652.jpg?v=1592988359937', CAST(N'2021-04-05T14:14:24.860' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Imagee] OFF
GO
SET IDENTITY_INSERT [dbo].[News] ON 

INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (1, N'Cách phối đồ với giày adidas cho nam và nữ đẹp ngất ngây', N'<div class="entry-content">
			<p>Hiện nay hãng giày Adidas ngày càng phát triển đi cùng với đó là cách phối màu đa dạng cũng khiến không ít bạn trẻ đau đầu tìm ra cách phối đồ sao cho hợp lý. Vậy thì cách phối đồ với giày Adidas như thế nào để có thể tạo được một phong cách riêng cho bản thân mà không bị trùng lặp. Hãy cùng Shop Giày Rep tìm hiểu một số <a title="Cách phối đồ với giày adidas" href="https://giayrep.vn/cach-phoi-do-voi-giay-adidas"><strong>cách phối đồ với giày adidas</strong></a> cho nam và nữ đẹp ngất ngây qua bài viết sau đây nhé!</p>
<div id="toc_container" class="no_bullets"><p class="toc_title">Mục Lục</p><ul class="toc_list"><li><a href="#Cach_phoi_do_voi_giay_Adidas_nam"><span class="toc_number toc_depth_1">1</span> Cách phối đồ với giày Adidas nam</a><ul><li><a href="#Ao_so_mi_quan_ngo"><span class="toc_number toc_depth_2">1.1</span> Áo sơ mi, quần ngố</a></li><li><a href="#Ao_so_mi_trang_quan_jean"><span class="toc_number toc_depth_2">1.2</span> Áo sơ mi trắng, quần jean</a></li><li><a href="#Giay_Adidas_ket_hop_voi_Vest_co_duoc_hay_khong"><span class="toc_number toc_depth_2">1.3</span> Giày Adidas kết hợp với Vest có được hay không?</a></li><li><a href="#Ao_phong_quan_ngo"><span class="toc_number toc_depth_2">1.4</span> Áo phông, quần ngố</a></li><li><a href="#Ao_phong_quan_jean"><span class="toc_number toc_depth_2">1.5</span> Áo phông, quần jean</a></li></ul></li><li><a href="#Cach_phoi_do_voi_giay_Adidas_nu"><span class="toc_number toc_depth_1">2</span> Cách phối đồ với giày Adidas nữ</a><ul><li><a href="#Ao_phong_cung_chan_vay_ngan"><span class="toc_number toc_depth_2">2.1</span> Áo phông cùng chân váy ngắn</a></li><li><a href="#Ao_tre_vai_ket_hop_voi_quan_short_jeans"><span class="toc_number toc_depth_2">2.2</span> Áo trễ vai kết hợp với quần short jeans</a></li><li><a href="#Ao_Croptop_ket_hop_cung_chan_vay_dai_xe"><span class="toc_number toc_depth_2">2.3</span> Áo Croptop kết hợp cùng chân váy dài xẻ</a></li><li><a href="#Ao_khoet_nguc_chu_V_va_chan_vay_bo"><span class="toc_number toc_depth_2">2.4</span> Áo khoét ngực chữ V và chân váy bò</a></li></ul></li><li><a href="#Nen_lua_chon_giay_Adidas_mau_gi_de_de_phoi_do"><span class="toc_number toc_depth_1">3</span> Nên lựa chọn giày Adidas màu gì để dễ phối đồ?</a></li></ul></div>
<h2 id="Cách_phối_đồ_với_giày_Adidas_nam" title="Cách phối đồ với giày Adidas nam"><span id="Cach_phoi_do_voi_giay_Adidas_nam">Cách phối đồ với giày Adidas nam</span></h2>
<p>Cách phối đồ với giày Adidas Nam quả thực không khó như nhiều bạn vẫn nghĩ, chỉ cần một chút sáng tạo và lựa chọn tông màu sao cho hợp lí là bạn đã có thể có được điểm cao trong mắt người đối diện rồi. Sự kết hợp của những đôi giày Adidas với những Item sau là không thể thiếu được, bạn có thể thay đổi phong cách hàng ngày của mình bằng những cách phối đồ đáng để tham khảo sau:</p>
<ul>
<li>
<h3><span id="Ao_so_mi_quan_ngo">Áo sơ mi, quần ngố</span></h3>
</li>
</ul>
<p>Với tình trạng thời tiết nắng nóng như hiện nay thì việc kết hợp một chiếc áo sơ mi sáng màu cùng một chiếc quần ngố trung tính là sự lựa chọn hoàn hảo, bạn không chỉ mang đến cảm giác lịch sự, mà còn thể hiện sự năng động và cuốn hút đối với mọi người, việc kết hợp cùng một số phụ kiện như đồng hồ đeo tay, cùng những đôi giày Adidas trắng bạn sẽ thật cá tính.</p>
<div id="attachment_2524" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2524" loading="lazy" class="wp-image-2524" title="Áo sơ mi, quần ngố kết hợp với giày adidas là lựa chọn tuyệt vời cho mùa hè" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-1-1.jpg" alt="Phối đồ với giày adidas nam, cách Phối đồ với giày adidas, Phối đồ với giày adidas, cách Phối đồ với giày adidas nữ, Phối đồ với giày adidas trắng, cách Phối đồ với giày adidas nam" width="600" height="1074" /><p id="caption-attachment-2524" class="wp-caption-text">Áo sơ mi, quần ngố kết hợp với giày adidas là lựa chọn tuyệt vời cho mùa hè</p></div>
<ul>
<li>
<h3><span id="Ao_so_mi_trang_quan_jean">Áo sơ mi trắng, quần jean</span></h3>
</li>
</ul>
<p>Đây là một cách phối đồ với giày Adidas mà bạn không thể bỏ qua, một phong cách được rất nhiều người ưa chuộng nhưng không bao giờ mất đi sự cuốn hút. Bạn có thể mix riêng phá cách hơn bằng những chiếc quần jeans rách nhẹ. Vào một ngày đẹp trời bạn phối một bộ đồ đơn giản mà vẫn tinh tế và lịch sự như vậy thì nhất định sẽ ghi điểm rất lớn đối với các bạn nữ.</p>
<ul>
<li>
<h3><span id="Giay_Adidas_ket_hop_voi_Vest_co_duoc_hay_khong">Giày Adidas kết hợp với Vest có được hay không?</span></h3>
</li>
</ul>
<p>Các quý ông thường chọn cho mình những bộ vest lịch lãm cùng những chiếc giày tây bóng loáng, đó là sự kết hợp kinh điển, nhưng bạn có thể thay đổi phong cách của mình bằng cách phối đồ với giày Adidas . Với sự kết hợp độc đáo này không làm mất đi sự thanh lịch của những bộ vest mà còn tạo cho bạn một phong cách trẻ trung năng động hơn hẳn. Đây là một ý tưởng không tồi, bạn nên thử thay đổi phong cách của mình đa dạng hơn nhé.</p>
<div id="attachment_2518" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2518" loading="lazy" class="wp-image-2518" title="Phối giày adidas với bộ đồ vest, tại sao lại không nhỉ?" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-3.jpg" alt="Phối đồ với giày adidas nam, cách Phối đồ với giày adidas, Phối đồ với giày adidas, cách Phối đồ với giày adidas nữ, Phối đồ với giày adidas trắng, cách Phối đồ với giày adidas nam" width="600" height="600" srcset="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-3.jpg 733w, https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-3-150x150.jpg 150w, https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-3-100x100.jpg 100w" sizes="(max-width: 600px) 100vw, 600px" /><p id="caption-attachment-2518" class="wp-caption-text">Phối giày adidas với bộ đồ vest, tại sao lại không nhỉ?</p></div>
<ul>
<li>
<h3><span id="Ao_phong_quan_ngo">Áo phông, quần ngố</span></h3>
</li>
</ul>
<p>Với những bạn ưa thích phong cách năng động thể thao thì đây là một sự kết hợp khá thu hút. Bạn có thể thay đổi màu sắc áo phông theo ý thích, cùng một chiếc quần ngố màu trung tính, Cùng với việc phối với một đôi giày Adidas trông bạn thật cá tính. Bạn có thể kết hợp giày với tất cổ cao để tạo điểm nhấn cho set đồ. Mùa hè đến rồi hãy chọn cho mình những set đồ năng động, cá tính để có những chuyến đi đáng nhớ với hội bạn thôi nào.</p>
<div id="attachment_2516" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2516" loading="lazy" class="wp-image-2516" title="Áo phông, quần ngố kết hợp với giày Adidas cho bạn nam cá tính" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-1.jpg" alt="Phối đồ với giày adidas nam, cách Phối đồ với giày adidas, Phối đồ với giày adidas, cách Phối đồ với giày adidas nữ, Phối đồ với giày adidas trắng, cách Phối đồ với giày adidas nam" width="600" height="516" /><p id="caption-attachment-2516" class="wp-caption-text">Áo phông, quần ngố kết hợp với giày Adidas cho bạn nam cá tính</p></div>
<ul>
<li>
<h3><span id="Ao_phong_quan_jean">Áo phông, quần jean</span></h3>
</li>
</ul>
<p>Với những bạn ưa thích phong cách bụi bặm thì không nên bỏ qua cách phối đồ này. Bạn có thể thể hiện cá tính riêng khi chọn cho mình cùng những chiếc áo sơ mi khoác bên ngoài.<br />
Ngoài ra bạn có thể mix phá cách cùng những chiếc áo ba lỗ cùng quần ngố thêm vào một chút màu sắc. tạo sự năng động trẻ trung. Với những bạn đam mê tập thể hình thì đây là set đồ rất nam tính, năng động. Hẳn là bạn sẽ thu hút được phái nữ.</p>
<div id="attachment_2520" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2520" loading="lazy" class="wp-image-2520" title="Áo phông và quần jean phối với giày adidas là quá &quot;chuẩn bài&quot; rồi đúng không nào" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-5.jpg" alt="Phối đồ với giày adidas nam, cách Phối đồ với giày adidas, Phối đồ với giày adidas, cách Phối đồ với giày adidas nữ, Phối đồ với giày adidas trắng, cách Phối đồ với giày adidas nam" width="600" height="320" srcset="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-5.jpg 1267w, https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-5-1024x546.jpg 1024w, https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-5-768x410.jpg 768w" sizes="(max-width: 600px) 100vw, 600px" /><p id="caption-attachment-2520" class="wp-caption-text">Áo phông và quần jean phối với giày adidas là quá &#8220;chuẩn bài&#8221; rồi đúng không nào</p></div>
<h2 id="Cách_phối_đồ_với_giày_Adidas_nữ" title="Cách phối đồ với giày Adidas nữ"><span id="Cach_phoi_do_voi_giay_Adidas_nu">Cách phối đồ với giày Adidas nữ</span></h2>
<p>Là phái đẹp nên những bộ đồ với nhiều mẫu mã đa dạng có phần giúp <strong>Cách phối đồ với giày Adidas</strong> nữ trở dễ dàng hơn rất nhiều. Nhưng nếu bạn chưa tìm được cách nào đẹp và hợp với mình nhất thì thì có thể tham khảo những gợi ý mà Shop Giày Rep đã cẩn thận chắt lọc giúp bạn dễ dàng lựa chọn nhé.</p>
<ul>
<li>
<h3><span id="Ao_phong_cung_chan_vay_ngan">Áo phông cùng chân váy ngắn</span></h3>
</li>
</ul>
<p>Các bạn nữ sẽ dịu dàng nữ tính mà không kém sự năng động khi kết hợp chân váy ngắn cùng giày adidas trắng. Đặc biệt là các bạn “nấm” nhé, đây là một trong những cách ăn gian chiều cao khá là hiệu quả.</p>
<div id="attachment_2521" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2521" loading="lazy" class="wp-image-2521" title="Áo phông cùng chân váy ngắn và giày adidas sẽ làm bạn nữ dịu dàng hơn đó" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-6.jpg" alt="Phối đồ với giày adidas nam, cách Phối đồ với giày adidas, Phối đồ với giày adidas, cách Phối đồ với giày adidas nữ, Phối đồ với giày adidas trắng, cách Phối đồ với giày adidas nam" width="600" height="895" /><p id="caption-attachment-2521" class="wp-caption-text">Áo phông cùng chân váy ngắn và giày adidas sẽ làm bạn nữ dịu dàng hơn đó</p></div>
<ul>
<li>
<h3><span id="Ao_tre_vai_ket_hop_voi_quan_short_jeans">Áo trễ vai kết hợp với quần short jeans</span></h3>
</li>
</ul>
<p>Với một phong cách sexy, các bạn nữ hẳn không bỏ qua được sự kết hợp này. Với một chiếc áo trễ vai họa tiết màu sắc đơn giản, kết hợp cùng một chiếc quần short jeans và đặc biệt không thể thiếu sự kết hợp cùng với giày Adidas trắng. sẽ khiến phong cách của bạn vừa quyến rũ mà vẫn năng động và tự tin.</p>
<div id="attachment_2522" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2522" loading="lazy" class="wp-image-2522" title="Áo trễ vai kết hợp với quần short jeans nhìn có phần bánh bèo" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-7.jpg" alt="Phối đồ với giày adidas nam, cách Phối đồ với giày adidas, Phối đồ với giày adidas, cách Phối đồ với giày adidas nữ, Phối đồ với giày adidas trắng, cách Phối đồ với giày adidas nam" width="600" height="893" /><p id="caption-attachment-2522" class="wp-caption-text">Áo trễ vai kết hợp với quần short jeans nhìn có phần bánh bèo</p></div>
<ul>
<li>
<h3><span id="Ao_Croptop_ket_hop_cung_chan_vay_dai_xe">Áo Croptop kết hợp cùng chân váy dài xẻ</span></h3>
</li>
</ul>
<p>Những cô nàng có thân hình chuẩn hẳn là rất thích những set đồ này. Một chiếc chân váy dài ôm giúp khoe được những đường cong của cơ thể, kết hợp cùng áo croptop sẽ khiến cho cô nàng khoe được những vòng eo “con kiến”, không thể thiếu là việc phối đồ với giày Adidas trắng tạo cho bạn một sự thoải mái, năng động. Bộ đồ rất thích hợp cho việc bạn dạo phố với những cuộc đi chơi cuối tuần sau những ngày làm việc mệt mỏi.</p>
<div id="attachment_2523" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2523" loading="lazy" class="wp-image-2523" title="Áo Croptop kết hợp cùng chân váy dài xẻ và giày adidas còn gì cá tính hơn" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-8.jpg" alt="Phối đồ với giày adidas nam, cách Phối đồ với giày adidas, Phối đồ với giày adidas, cách Phối đồ với giày adidas nữ, Phối đồ với giày adidas trắng, cách Phối đồ với giày adidas nam" width="600" height="728" /><p id="caption-attachment-2523" class="wp-caption-text">Áo Croptop kết hợp cùng chân váy dài xẻ và giày adidas còn gì cá tính hơn</p></div>
<ul>
<li>
<h3><span id="Ao_khoet_nguc_chu_V_va_chan_vay_bo">Áo khoét ngực chữ V và chân váy bò</span></h3>
</li>
</ul>
<p>Đây là một “Item “ cho những nàng bánh bèo ưa thích phong cách công chúa, nhẹ nhàng và đáng yêu. Với sự kết hợp một chiếc áo chữ V tay bèo, màu trắng, một chiếc chân váy bò xòe hoặc ôm dáng cùng cách phối đồ với giày adidas trắng tạo cho bạn điểm nhấn cho trang phục. Sét đồ này nhất định sẽ giúp bạn ghi điểm trong mắt “crush”, hãy lên đồ và đi chơi thôi nào…</p>
<p>Trên đây là một trong những gợi ý của Giày Rep trong cách phối đồ với giày Adidas trắng. Bạn có thể tham khảo và cho chúng mình thêm những ý kiến về cách phối đồ khác nữa nha!</p>
<h2 id="Nên_lựa_chọn_giày_Adidas_màu_gì_để_dễ_phối_đồ" title="Nên lựa chọn giày Adidas màu gì để dễ phối đồ"><span id="Nen_lua_chon_giay_Adidas_mau_gi_de_de_phoi_do">Nên lựa chọn giày Adidas màu gì để dễ phối đồ?</span></h2>
<p>Để dễ dàng phối đồ hơn bạn nên chọn những đôi <a title="Giày Adidas" href="https://giayrep.vn/giay-adidas/" target="_blank" rel="noopener noreferrer">Giày Adidas</a> có màu trắng, với tông màu sáng sẽ giúp bạn thể hiện được sự giản dị, hài hòa mà vẫn giữ được nét hiện đại. Những đôi giày Adidas màu trắng sẽ cực kì &#8220;xoẹt tông&#8221; khi được mix chung với những bộ quần áo khác nhau. Nhưng có một nhược điểm là khi mang những đôi giày màu trắng vào lúc trời mưa, hoặc đi vào nơi có nhiều bùn đất thì nó rất dễ bị bẩn. Để khắc phục tình trạng này bạn nên phòng sẵn trong người dụng cụ làm sạch giày khi cần thiết nhé.</p>
<div id="attachment_2532" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2532" loading="lazy" class="wp-image-2532" title="Nên chọn những đôi giày Adidas có màu trắng sẽ giúp bạn dễ chọn đồ phối hơn" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-10.jpg" alt="Phối đồ với giày adidas nam, cách Phối đồ với giày adidas, Phối đồ với giày adidas, cách Phối đồ với giày adidas nữ, Phối đồ với giày adidas trắng, cách Phối đồ với giày adidas nam" width="600" height="336" srcset="https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-10.jpg 958w, https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas-10-768x430.jpg 768w" sizes="(max-width: 600px) 100vw, 600px" /><p id="caption-attachment-2532" class="wp-caption-text">Nên chọn những đôi giày Adidas có màu trắng sẽ giúp bạn dễ chọn đồ phối hơn</p></div>
<p>Trong trường hợp bạn vẫn chưa sở hữu được một đôi giày Adidas ưng ý thì hãy truy cập ngay vào Shop để lựa chọn một đôi cho mình nhé.</p>
<p>Trên đây là bài viết chia sẻ <strong>cách phối đồ với giày adidas</strong> cho nam và nữ đẹp nhất mà <a title="Shop Giày Rep" href="https://giayrep.vn/" target="_blank" rel="noopener noreferrer">Shop Giày Rep</a> tổng hợp lại, nếu có đóng góp ý kiến nào khác bạn đọc có thể để lại bình luận phía dưới để chúng tôi giải đáp.</p>
					</div>', N'https://giayrep.vn/wp-content/uploads/2020/06/cach-phoi-do-voi-giay-adidas.jpg', CAST(N'2021-04-09T00:00:00.000' AS DateTime), CAST(N'2021-04-09T00:00:00.000' AS DateTime))
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (2, N'Khái niệm giày replica là gì? Có nên mua hay không?', N'<div class="entry-content">
			<p>Việc sở hữu một phiên bản Sneaker chính hãng đang làm mưa, làm gió trên thị trường như Stan Smith, Ultra Boost, Adidas Yeezy, Nike Air, Balenciaga Triple S… là điều đáng mơ ước của nhiều bạn trẻ, vì không phải ai cũng có đủ điều kiện để mua cho mình một đôi. Cũng chính vì điều này nên nhiều bạn tìm đến lựa chọn khác đó là mua giày thuộc hàng replica. Nhưng các bạn lại không biết đôi giày mình chọn mua có chất lượng, mẫu mã đúng là giày replica thật không? Nếu bạn có những câu hỏi <a title="Khái niệm giày replica là gì" href="https://giayrep.vn/khai-niem-giay-replica-la-gi"><strong>Khái niệm giày replica là gì</strong></a>? Có nên mua hay không? thì hãy cùng Shop Giày Rep phân tích chi tiết, chuyên sâu qua bài viết sau đây nhé!</p>
<div id="attachment_2577" style="width: 810px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2577" loading="lazy" class="wp-image-2577" src="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong.jpg" alt="giày replica, giày rep +, giày rep, giày replica là gì, giày replica 1 1 là gì, giày replica 1 1, giày replica và super fake, replica giày, giày hàng replica là gì, có nên mua giày replica, giày hàng replica, mua giày replica, mua giày replica ở đâu, giày replica và rep 11, giày rep 1, giày replica giá rẻ, hàng giày rep, giày replica 11, giày replica và auth, giày replica thường là gì, giày replica có mấy loại, giày replica có tốt không" width="800" height="797" srcset="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong.jpg 931w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-150x150.jpg 150w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-768x765.jpg 768w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-100x100.jpg 100w" sizes="(max-width: 800px) 100vw, 800px" /><p id="caption-attachment-2577" class="wp-caption-text">Giày Replica là gì? là câu hỏi mà nhiều bạn yêu thích Sneaker quan tâm</p></div>
<div id="toc_container" class="no_bullets"><p class="toc_title">Mục Lục</p><ul class="toc_list"><li><a href="#Khai_niem_giay_replica_la_gi_Co_may_loai"><span class="toc_number toc_depth_1">1</span> Khái niệm giày replica là gì? Có mấy loại?</a><ul><li><a href="#Giay_replica_la_gi"><span class="toc_number toc_depth_2">1.1</span> Giày replica là gì?</a></li><li><a href="#Giay_replica_co_may_loai"><span class="toc_number toc_depth_2">1.2</span> Giày replica có mấy loại?</a><ul><li><a href="#Giay_rep_la_gi"><span class="toc_number toc_depth_3">1.2.1</span> Giày rep là gì?</a></li><li><a href="#Giay_Rep_la_gi"><span class="toc_number toc_depth_3">1.2.2</span> Giày Rep + là gì?</a></li><li><a href="#Giay_rep_11_la_gi"><span class="toc_number toc_depth_3">1.2.3</span> Giày rep 1:1 là gì?</a></li></ul></li></ul></li><li><a href="#Co_nen_mua_giay_replica_hay_khong"><span class="toc_number toc_depth_1">2</span> Có nên mua giày replica hay không?</a></li><li><a href="#So_sanh_giay_replica_va_dong_giay_khac_tren_thi_truong"><span class="toc_number toc_depth_1">3</span> So sánh giày replica và dòng giày khác trên thị trường</a><ul><li><a href="#So_sanh_giay_replica_va_super_fake"><span class="toc_number toc_depth_2">3.1</span> So sánh giày replica và super fake</a></li><li><a href="#So_Sanh_giay_replica_va_rep_11"><span class="toc_number toc_depth_2">3.2</span> So Sánh giày replica và rep 1:1</a></li></ul></li><li><a href="#Dia_chi_Shop_giay_Replica_uy_tin_o_Ha_noi_va_TPHCM"><span class="toc_number toc_depth_1">4</span> Địa chỉ Shop giày Replica uy tín ở Hà nội và TPHCM</a></li></ul></div>
<h2 id="Khái_niệm_giày_replica_là_gì_Có_mấy_loại" title="Khái niệm giày replica là gì Có mấy loại"><span id="Khai_niem_giay_replica_la_gi_Co_may_loai">Khái niệm giày replica là gì? Có mấy loại?</span></h2>
<h3 id="Giày_replica_là_gì" style="padding-left: 40px;" title="Giày replica là gì"><span id="Giay_replica_la_gi"><span style="color: #ff6600;">Giày replica là gì?</span></span></h3>
<p>Giày Replica có thể dịch theo nghĩa chuẩn là giày “Bản Sao” đây là dòng giày được sao chép một cách tỉ mỉ đến từng chi tiết nhỏ. Về mẫu mã, chất liệu đều giống hàng chính hãng gần như 100%. Ở Châu Âu loại giày này được sản xuất dành cho những câu lạc bộ hoặc một vận động viên. Hiện nay giày Replica được bày bán rất rộng rãi ở nhiều cửa hàng và được nhiều bạn trẻ ưa thích lựa chọn vì giá rẻ mà lại có độ bền cao.</p>
<div id="attachment_2582" style="width: 810px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2582" loading="lazy" class="wp-image-2582" src="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-6.jpg" alt="giày replica, giày rep +, giày rep, giày replica là gì, giày replica 1 1 là gì, giày replica 1 1, giày replica và super fake, replica giày, giày hàng replica là gì, có nên mua giày replica, giày hàng replica, mua giày replica, mua giày replica ở đâu, giày replica và rep 11, giày rep 1, giày replica giá rẻ, hàng giày rep, giày replica 11, giày replica và auth, giày replica thường là gì, giày replica có mấy loại, giày replica có tốt không" width="800" height="797" srcset="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-6.jpg 896w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-6-150x150.jpg 150w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-6-768x765.jpg 768w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-6-100x100.jpg 100w" sizes="(max-width: 800px) 100vw, 800px" /><p id="caption-attachment-2582" class="wp-caption-text">Hình ảnh giày Yeezy 350 Replica 1:1 tại Shop Giày Rep</p></div>
<h3 id="Giày_replica_có_mấy_loại" style="padding-left: 40px;" title="Giày replica có mấy loại"><span id="Giay_replica_co_may_loai"><span style="color: #ff6600;">Giày replica có mấy loại?</span></span></h3>
<p>Phát triển đi đôi với nhu cầu sử dụng của cộng đồng người yêu thích sneaker nói chung và hàng replica nói riêng, nhiều xưởng <strong>giày replica</strong> đã cho ra đời những loại giày khác nhau như giày Rep, Rep+, Rep 1:1. Để biết được chi tiết các loại giày này khác nhau như thế nào, các bạn hãy cùng tìm hiểu khái niệm từng loại sau nhé:</p>
<ul>
<li>
<h4 id="Giày_rep_là_gì" title="Giày rep là gì"><span id="Giay_rep_la_gi"><span style="color: #ff9900;">Giày rep là gì?</span></span></h4>
</li>
</ul>
<p>Giày Rep hay còn được biết đến với cái tên đầy đủ là giày Replica thường. Giày rep được sao chép tỉ mỉ về hình thức, chất liệu giống với hàng chính hãng nhất có thể.</p>
<ul>
<li>
<h4 id="Giày_Rep_+_là_gì" title="Giày Rep + là gì"><span id="Giay_Rep_la_gi"><span style="color: #ff9900;">Giày Rep + là gì?</span></span></h4>
</li>
</ul>
<p>Giày Rep + hay còn được gọi là Rep Plus dùng để chỉ giày hàng replica nhưng được nhà buôn đặt xưởng sản xuất theo số lượng lớn và được cam kết chất lượng cao hơn hàng Rep thường. Chính vì thế nên chỉ khi nào có yêu cầu từ phía nhà buôn thì xưởng mới sản xuất loại giày này.</p>
<ul>
<li>
<h4 id="Giày_rep_1:1_là_gì" title="Giày rep 1:1 là gì"><span id="Giay_rep_11_la_gi"><span style="color: #ff9900;">Giày rep 1:1 là gì?</span></span></h4>
</li>
</ul>
<p>Giày Rep 1:1 hay Replica 1:1 là giày được sản xuất theo tỉ lệ 1:1 với hàng chính hãng. Trong quá gia công sử dụng những chất liệu giống như giày chính hãng. Đây được cho là hàng phân khúc cao cấp của giày replica nên giá thành cũng cao hơn so với giày replica thông thường.</p>
<div id="attachment_2580" style="width: 810px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2580" loading="lazy" class="wp-image-2580" src="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-4.jpg" alt="giày replica, giày rep +, giày rep, giày replica là gì, giày replica 1 1 là gì, giày replica 1 1, giày replica và super fake, replica giày, giày hàng replica là gì, có nên mua giày replica, giày hàng replica, mua giày replica, mua giày replica ở đâu, giày replica và rep 11, giày rep 1, giày replica giá rẻ, hàng giày rep, giày replica 11, giày replica và auth, giày replica thường là gì, giày replica có mấy loại, giày replica có tốt không" width="800" height="1067" /><p id="caption-attachment-2580" class="wp-caption-text">Đôi Nike Air Rep 1:1 khó có thể phân biệt bằng mắt thường</p></div>
<h2 id="Có_nên_mua_giày_replica_hay_không" title="Có nên mua giày replica hay không"><span id="Co_nen_mua_giay_replica_hay_khong">Có nên mua giày replica hay không?</span></h2>
<p>Nếu mức thu nhập của bạn còn chưa cao, việc bỏ một khoản tiền lớn ra để mua một đôi giày chính hãng (auth) lên đến 8 &#8211; 15tr là điều khó khăn và xa xỉ thì có thể trả lời cho câu hỏi có nên mua giày replica không là CÓ. Vì khi mà giày replica bạn sẽ đảm bảo được những yếu tố như:</p>
<ul>
<li><span style="color: #008000;">√</span> Hình thức, mẫu mã đẹp sang chảnh</li>
<li><span style="color: #008000;">√</span> Chất liệu như hàng chính hãng</li>
<li><span style="color: #008000;">√</span> Có tên tuổi thương hiệu</li>
</ul>
<div id="attachment_2578" style="width: 810px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2578" loading="lazy" class="wp-image-2578" src="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-2.jpg" alt="giày replica, giày rep +, giày rep, giày replica là gì, giày replica 1 1 là gì, giày replica 1 1, giày replica và super fake, replica giày, giày hàng replica là gì, có nên mua giày replica, giày hàng replica, mua giày replica, mua giày replica ở đâu, giày replica và rep 11, giày rep 1, giày replica giá rẻ, hàng giày rep, giày replica 11, giày replica và auth, giày replica thường là gì, giày replica có mấy loại, giày replica có tốt không" width="800" height="793" srcset="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-2.jpg 931w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-2-150x150.jpg 150w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-2-768x761.jpg 768w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-2-100x100.jpg 100w" sizes="(max-width: 800px) 100vw, 800px" /><p id="caption-attachment-2578" class="wp-caption-text">Đôi MLB Boston Replica được thiết kế chính xác đến từng chi tiết</p></div>
<h2 id="So_sánh_giày_replica_và_dòng_giày_khác_trên_thị_trường" title="So sánh giày replica và dòng giày khác trên thị trường"><span id="So_sanh_giay_replica_va_dong_giay_khac_tren_thi_truong">So sánh giày replica và dòng giày khác trên thị trường</span></h2>
<h3 id="So_sánh_giày_replica_và_super_fake" style="padding-left: 40px;" title="So sánh giày replica và super fake"><span id="So_sanh_giay_replica_va_super_fake"><span style="color: #ff6600;">So sánh giày replica và super fake</span></span></h3>
<p>Giày Super Fake (sf) là sản phẩm được sao chép giống khoảng 70 &#8211; 85% so với đôi giày chính hãng của nhà sản xuất, trong khi giày replica có độ chính xác có hơn đến 90%. Do vậy nếu so sánh giá cả, chất liệu, thiết kế thì <strong>Giày Replica</strong> sẽ nhỉnh hơn Giày Super Fake. Trong trường hợp bạn thấy không đủ kinh tế để mua một đôi giày Replica thì lựa chọn giày Super Fake cũng không tôi chút nào.</p>
<h3 id="So_Sánh_giày_replica_và_rep_1:1" style="padding-left: 40px;" title="So Sánh giày replica và rep 1:1"><span id="So_Sanh_giay_replica_va_rep_11"><span style="color: #ff6600;">So Sánh giày replica và rep 1:1</span></span></h3>
<p>Với độ chính xác của giày replica là 90% và của Replica 1:1 (rep 11) là 95% nên giày Rep 1:1 sẽ có giá cao hơn nếu bán trên thị trường. Nhưng đi đôi với đó là một đôi giày chất lượng, với thiết kế tinh xảo đến từng chi tiết nhỏ. Hãy lựa chọn giày replica 1:1 nếu như bạn đang cần một đôi giày với độ chính xác cao và bền.</p>
<div id="attachment_2584" style="width: 706px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2584" loading="lazy" class="wp-image-2584 size-full" src="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-7.jpg" alt="giày replica, giày rep +, giày rep, giày replica là gì, giày replica 1 1 là gì, giày replica 1 1, giày replica và super fake, replica giày, giày hàng replica là gì, có nên mua giày replica, giày hàng replica, mua giày replica, mua giày replica ở đâu, giày replica và rep 11, giày rep 1, giày replica giá rẻ, hàng giày rep, giày replica 11, giày replica và auth, giày replica thường là gì, giày replica có mấy loại, giày replica có tốt không" width="696" height="929" /><p id="caption-attachment-2584" class="wp-caption-text">Với thiết kế chính xác đến từng chi tiết nhỏ, Giày replica 1:1 sẽ không làm bạn thất vọng</p></div>
<h2 id="Địa_chỉ_Shop_giày_Replica_uy_tín_ở_Hà_nội_và_TPHCM" title="Địa chỉ Shop giày Replica uy tín ở Hà nội và TPHCM"><span id="Dia_chi_Shop_giay_Replica_uy_tin_o_Ha_noi_va_TPHCM">Địa chỉ Shop giày Replica uy tín ở Hà nội và TPHCM</span></h2>
<p>Hiện nay trên thị trường có rất nhiều shop giày replica, khiến nhiều bạn trẻ không khỏi đắn đo suy nghĩ về việc lựa chọn <a title="Shop Giày Replica Uy Tín" href="https://giayrep.vn/" target="_blank" rel="noopener noreferrer">Shop Giày Replica Uy Tín</a> ở cả Hà Nội và Tp HCM. Nếu không có kiến thức chuyên sâu về giày replica thì khi chọn giày các bạn sẽ gặp một số vấn đề lớn như việc mua phải giày kém chất lượng hơn replica, chất liệu mẫu mã không được tốt&#8230;</p>
<p>Nắm bắt được mối quan tâm, lo ngại của các bạn trẻ Shop Giày Replica chúng tôi không ngừng cập nhật những mẫu giày chất lượng, chuẩn replica, hợp &#8220;trend&#8221; của các hãng giày nổi tiếng như Adidas, Nike, Vans, Converse, <a title="Giày Balenciaga" href="https://giayrep.vn/giay-balenciaga-rep" target="_blank" rel="noopener noreferrer">Giày Balenciaga</a>… để các bạn trẻ có được sự lựa chọn phù hợp với cá tính, phong cách của chính mình.</p>
<p>Khi mua giày tại Shop Giày Rep qua hình thức ship COD các bạn có thể hoàn toàn yên tâm vì bạn sẽ được kiểm tra giày trước khi thanh toán. Trong trường hợp giày chưa qua sử dụng, còn nguyên tem, bạn sẽ được đổi trả trong vòng 7 ngày.</p>
<div id="attachment_2579" style="width: 810px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2579" loading="lazy" class="wp-image-2579" src="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-3.jpg" alt="giày replica, giày rep +, giày rep, giày replica là gì, giày replica 1 1 là gì, giày replica 1 1, giày replica và super fake, replica giày, giày hàng replica là gì, có nên mua giày replica, giày hàng replica, mua giày replica, mua giày replica ở đâu, giày replica và rep 11, giày rep 1, giày replica giá rẻ, hàng giày rep, giày replica 11, giày replica và auth, giày replica thường là gì, giày replica có mấy loại, giày replica có tốt không" width="800" height="798" srcset="https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-3.jpg 927w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-3-150x150.jpg 150w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-3-768x766.jpg 768w, https://giayrep.vn/wp-content/uploads/2020/06/Khai-niem-giay-replica-la-gi-Co-nen-mua-hay-khong-3-100x100.jpg 100w" sizes="(max-width: 800px) 100vw, 800px" /><p id="caption-attachment-2579" class="wp-caption-text">MLB Mickey Replica mà nhiều bạn ưa thích</p></div>
<p>Hy vọng qua bài viết trên bạn đã giải đáp được thắc mắc <strong>Khái niệm giày replica là gì</strong>? Có nên mua hay không? và chọn cho mình được địa chỉ mua giày replica uy tín. Nếu có đóng góp ý kiến thêm cho bài viết bạn đọc có thể để lại bình luận, hoặc inbox trực tiếp cho Shop để được giải kịp thời nhé!</p>
					</div>', N'https://giayrep.vn/wp-content/uploads/2020/04/Shop-Giay-Rep-Chat-Luong-Gia-Tot-3.jpg', CAST(N'2021-04-09T00:00:00.000' AS DateTime), CAST(N'2021-04-09T00:00:00.000' AS DateTime))
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (3, N'4 cách khắc phục giày bị rộng 1 size ngay tại nhà', N'<div class="entry-content">
			<p>Khi mua giày online không ít bạn đã gặp phải trường hợp giày bị rộng 1 size hơn so với thông thường, điều này xảy ra bởi các hãng giày sẽ có kích thước số đo chân theo size khác nhau, nên nếu không chú ý kỹ sẽ rất dễ mắc phải. Nhưng bạn cũng đừng lo lắng quá bởi bài viết sau đây <a title="shop giày rep" href="https://giayrep.vn/" target="_blank" rel="noopener noreferrer">shop giày rep</a> sẽ bật mí giúp bạn 4 cách khắc phục giày bị rộng 1 size ngay tại nhà đơn giản mà vô cùng hiệu quả nhé.</p>
<div id="toc_container" class="no_bullets"><p class="toc_title">Mục Lục</p><ul class="toc_list"><li><a href="#Cach_khac_phuc_giay_bi_rong_1_size"><span class="toc_number toc_depth_1">1</span> Cách khắc phục giày bị rộng 1 size</a><ul><li><a href="#1_Mang_tat_day_hon_khi_di_giay"><span class="toc_number toc_depth_2">1.1</span> 1. Mang tất dày hơn khi đi giày</a></li><li><a href="#2_Su_dung_giay"><span class="toc_number toc_depth_2">1.2</span> 2. Sử dụng giấy</a></li><li><a href="#3_Dung_mieng_lot_giay"><span class="toc_number toc_depth_2">1.3</span> 3. Dùng miếng lót giày</a></li><li><a href="#4_Dung_mieng_lot_co_chan"><span class="toc_number toc_depth_2">1.4</span> 4. Dùng miếng lót cổ chân</a></li></ul></li><li><a href="#Cach_lam_giay_rong_ra_1_size"><span class="toc_number toc_depth_1">2</span> Cách làm giày rộng ra 1 size</a></li></ul></div>
<h2 id="Cách_khắc_phục_giày_bị_rộng_1_size" title="Cách khắc phục giày bị rộng 1 size"><span id="Cach_khac_phuc_giay_bi_rong_1_size"><span style="color: #0000ff;">Cách khắc phục giày bị rộng 1 size</span></span></h2>
<p>Có rất nhiều <a title="cách khắc phục giày bị rộng 1 size" href="https://giayrep.vn/cach-khac-phuc-giay-bi-rong-1-size">cách khắc phục giày bị rộng 1 size</a> vô cùng đơn giản mà hiệu quả, bạn đọc hãy cùng tìm hiểu chi tiết qua 5 cách sau đây:</p>
<h3 style="text-align: left;"><span id="1_Mang_tat_day_hon_khi_di_giay"><span style="color: #ff6600;">1. Mang tất dày hơn khi đi giày</span></span></h3>
<p>Khi mua tất chân bạn nên chọn những đôi tất dày hơn thông thường, nó sẽ giúp chân bạn tăng kích thước hơn. Trong trường hợp không chọn mua được đôi tất dày bạn cũng có thể sử dụng nhiều đôi tất 1 lúc, từ 2-3 đôi là hợp lí nhất.</p>
<div id="attachment_2836" style="width: 810px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2836" loading="lazy" class="wp-image-2836" src="https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size.jpg" alt="giày bị rộng 1 size, có nên mang giày rộng hơn 1 size, giày rộng hơn 1 size, giày thể thao bị rộng 1 size, lót giày rộng size, cách làm giày rộng ra 1 size, đi giày rộng hơn 1 size, giày bị rộng 2 size, cách mang giày size rộng, mang giày size rộng" width="800" height="445" /><p id="caption-attachment-2836" class="wp-caption-text">Mang tất nhiều hơn cũng là một cách đơn giản</p></div>
<p>Tuy tiện lợi và thông dụng là thế nhưng cách khắc phục giày rộng hơn 1 size này lại có 1 yếu điểm là khi vào mùa hè sẽ rất nóng, tạo cảm giác khó chịu và ra nhiều mồ hôi chân dẫn đến hôi chân. Nên bạn cũng có thể cân nhắc khi chọn phương pháp này.</p>
<h3><span id="2_Su_dung_giay"><span style="color: #ff6600;">2. Sử dụng giấy</span></span></h3>
<p>Cách này cũng rất tiện lợi và hiệu quả, để thực hiện bạn có thể chọn khăn giấy, giấy báo nhét vào phần mũi giày, cách này hữu hiệu nhất hơi bạn nào bị thừa chiều dài chân.</p>
<div id="attachment_2837" style="width: 810px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2837" loading="lazy" class="wp-image-2837" src="https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-2.jpg" alt="giày bị rộng 1 size, có nên mang giày rộng hơn 1 size, giày rộng hơn 1 size, giày thể thao bị rộng 1 size, lót giày rộng size, cách làm giày rộng ra 1 size, đi giày rộng hơn 1 size, giày bị rộng 2 size, cách mang giày size rộng, mang giày size rộng" width="800" height="577" srcset="https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-2.jpg 881w, https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-2-768x554.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" /><p id="caption-attachment-2837" class="wp-caption-text">Không quá khó để bạn có thể kiếm giấy và nhét vào đôi giày bị rộng</p></div>
<p>Yếu điểm của cách này là nếu bạn cần dùng giày để đi đường dài sẽ gây cảm giác khó chịu. Bạn nên tính toán việc sử dụng cách này trước khi đi nhé.</p>
<h3><span id="3_Dung_mieng_lot_giay"><span style="color: #ff6600;">3. Dùng miếng lót giày</span></span></h3>
<p>Sử dụng miếng lót giày rộng size sẽ giúp chân bạn được đầy hơn. Lưu ý bạn nên thử lót giày trước khi mua để đảm bảo phù hợp với chân bạn, bởi những thương hiệu giày lớn như Adidas, Nike, Mlb đều có bán miếng lót riêng.</p>
<p><img loading="lazy" class="wp-image-2834 aligncenter" src="https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-3.jpg" alt="giày bị rộng 1 size, có nên mang giày rộng hơn 1 size, giày rộng hơn 1 size, giày thể thao bị rộng 1 size, lót giày rộng size, cách làm giày rộng ra 1 size, đi giày rộng hơn 1 size, giày bị rộng 2 size, cách mang giày size rộng, mang giày size rộng" width="800" height="814" /></p>
<h3><span id="4_Dung_mieng_lot_co_chan"><span style="color: #ff6600;">4. Dùng miếng lót cổ chân</span></span></h3>
<p>Bạn có thể dán 1 miếng đệm nhỏ, mỏng ở cổ chân để làm chật giày hơn, cách này bạn cần lưu ý nếu như bị phồng chân hay thấy khó chịu thì không nên dùng nữa.</p>
<div id="attachment_2835" style="width: 725px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2835" loading="lazy" class="wp-image-2835 size-full" src="https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-4.jpg" alt="giày bị rộng 1 size, có nên mang giày rộng hơn 1 size, giày rộng hơn 1 size, giày thể thao bị rộng 1 size, lót giày rộng size, cách làm giày rộng ra 1 size, đi giày rộng hơn 1 size, giày bị rộng 2 size, cách mang giày size rộng, mang giày size rộng" width="715" height="716" srcset="https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-4.jpg 715w, https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-4-150x150.jpg 150w, https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-4-100x100.jpg 100w" sizes="(max-width: 715px) 100vw, 715px" /><p id="caption-attachment-2835" class="wp-caption-text">Có miếng lót cổ chân này thì giày có rộng 2 size đi chăng nữa cũng là điều đơn giản</p></div>
<h2 id="Cách_làm_giày_rộng_ra_1_size" title="Cách làm giày rộng ra 1 size"><span id="Cach_lam_giay_rong_ra_1_size"><span style="color: #0000ff;">Cách làm giày rộng ra 1 size</span></span></h2>
<p>Trái ngược với việc khắc phục giày bị rộng 1 size thì cũng có nhiều bạn gặp phải trường hợp mua phải giày size nhỏ quá dẫn đến cần làm giày rộng ra 1 size. Để thực hiện hiệu quả bạn cần làm theo 3 bước sau đây:</p>
<p>Bước 1: Đổ nước vào 2 túi bóng chắc chắn và được bịt kín lại.<br />
Bước 2: Bạn đặt 2 túi nước vào trong giày, điều chỉnh vào bộ phận mà bạn muốn rộng.<br />
Bước 3: Để đôi giày bạn vừa thực hiện các bước trên vào ngăn đông đá tủ lạnh trong 24h, sau đó lấy ra khỏi tủ và chờ cho đá tan đi. Phần đông đá có tác dụng như một chiếc khôn to, chắc chắn giúp nới lỏng đôi giày của bạn.</p>
<p><strong>Lời kết</strong></p>
<p>Trên đây là những cách đơn đơn giản nhất để bạn có thể khắc phục được tình trạng giày bị rộng 1 size đơn giản nhất mà ai cũng có thể thực hiện được. Nếu có đóng góp thêm về cách thức làm rộng size giày nào khác bạn có thể để lại bình luận phía dưới nhé!</p>
					</div>', N'https://giayrep.vn/wp-content/uploads/2020/11/cach-khac-phuc-giay-bi-rong-1-size-5.jpg', CAST(N'2021-04-09T00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (4, N'Đôi giày balenciaga nặng bao nhiêu kg? Bật mí cân nặng chính xác', N'<div class="entry-content">
			<p>Sở hữu một đôi giày mang thương hiệu nổi tiếng Balenciaga luôn là điều mong ước của nhiều bạn yêu thích Sneaker. Với thiết kế hầm hố độc đáo như vậy nên không ít bạn thắc mắc liệu một đôi <a title="giày balenciaga nặng bao nhiêu kg" href="https://giayrep.vn/giay-balenciaga-nang-bao-nhieu-kg"><strong>giày Balenciaga nặng bao nhiêu kg</strong></a>? Việc mang đôi giày này bên người thì cân nặng liệu có phải sẽ là một trở ngại lớn khi đi ra ngoài? Hãy cùng Shop Giày Rep tìm hiểu chi tiết qua bài viết sau đây nhé!</p>
<div id="attachment_2648" style="width: 802px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2648" loading="lazy" class="wp-image-2648" src="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-5.jpg" alt="giày balenciaga nặng bao nhiêu kg, balenciaga nặng bao nhiêu kg, balenciaga triple s nặng bao nhiêu kg, giày balenciaga nặng bao nhiêu, balenciaga track nặng bao nhiêu, balenciaga real nặng bao nhiêu, đôi balenciaga nặng bao nhiêu kg, balenciaga real nặng bao nhiêu kg, balenciaga rep nặng bao nhiêu, đôi balenciaga nặng bao nhiêu" width="792" height="908" srcset="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-5.jpg 1117w, https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-5-894x1024.jpg 894w, https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-5-768x880.jpg 768w" sizes="(max-width: 792px) 100vw, 792px" /><p id="caption-attachment-2648" class="wp-caption-text">Cùng tìm hiểu một đôi balenciaga nặng bao nhiêu qua bài viết sau nhé!</p></div>
<div id="toc_container" class="no_bullets"><p class="toc_title">Mục Lục</p><ul class="toc_list"><li><a href="#Thuong_hieu_giay_Balenciaga"><span class="toc_number toc_depth_1">1</span> Thương hiệu giày Balenciaga</a></li><li><a href="#Giay_Balenciaga_nang_bao_nhieu_kg"><span class="toc_number toc_depth_1">2</span> Giày Balenciaga nặng bao nhiêu kg</a><ul><li><a href="#Balenciaga_Triple_S_nang_bao_nhieu_kg"><span class="toc_number toc_depth_2">2.1</span> Balenciaga Triple S nặng bao nhiêu kg?</a></li><li><a href="#Balenciaga_Track_nang_bao_nhieu"><span class="toc_number toc_depth_2">2.2</span> Balenciaga Track nặng bao nhiêu?</a></li><li><a href="#Balenciaga_Trainer_nang_bao_nhieu_kg"><span class="toc_number toc_depth_2">2.3</span> Balenciaga Trainer nặng bao nhiêu kg?</a></li></ul></li><li><a href="#1_doi_giay_nang_bao_nhieu_kg"><span class="toc_number toc_depth_1">3</span> 1 đôi giày nặng bao nhiêu kg</a></li><li><a href="#Mua_giay_balenciaga_o_dau_uy_tin_gia_re"><span class="toc_number toc_depth_1">4</span> Mua giày balenciaga ở đâu uy tín giá rẻ</a></li></ul></div>
<h2 id="Thương_hiệu_giày_Balenciaga" title="Thương hiệu giày Balenciaga"><span id="Thuong_hieu_giay_Balenciaga">Thương hiệu giày Balenciaga</span></h2>
<p>Balenciaga là thương hiệu thời trang nổi tiếng tại Châu Âu, được thành lập vào năm 1919. Balenciaga được lấy trong tên của nhà sáng lập Cristóbal Balenciaga, ông là nhà thiết kế thời trang người Tây Ban Nha gốc Basque.</p>
<p>Năm 1937 do cuộc nội chiến xảy ra nên Balenciaga đã chuyển trụ sở đến Paris &#8211; Pháp, cũng từ thời điểm này trở đi mà thương hiệu Balenciaga được nhiều người biết đến.</p>
<p>Ngày nay mỗi khi nhắc đến Balenciaga người ta sẽ nghĩ ngay đến những trang phục đắt tiền, những thiết kế phá cách đầy tinh tế và hiện đại. Trong đó có thể kể đến một số thiết kế mang tính cách mạng như đầm trống Barrel Line, đầm Sack Dress, áo khoác Ballon Jacket.</p>
<div id="attachment_2650" style="width: 1190px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2650" loading="lazy" class="wp-image-2650 size-full" src="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-7.jpg" alt="balenciaga triple s nặng bao nhiêu kg, giày balenciaga nặng bao nhiêu, balenciaga track nặng bao nhiêu, balenciaga real nặng bao nhiêu, đôi balenciaga nặng bao nhiêu kg, balenciaga real nặng bao nhiêu kg, balenciaga rep nặng bao nhiêu, đôi balenciaga nặng bao nhiêu" width="1180" height="659" srcset="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-7.jpg 1180w, https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-7-1024x572.jpg 1024w, https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-7-768x429.jpg 768w" sizes="(max-width: 1180px) 100vw, 1180px" /><p id="caption-attachment-2650" class="wp-caption-text">Thương hiệu Balenciaga trải qua nhiều biến cố mới có được như ngày hôm nay</p></div>
<p>Ngoài việc quan tâm đến <strong>giày balenciaga nặng bao nhiêu kg</strong> cũng có nhiều người nói 1 đôi giày Balenciaga có giá trên trời bởi mức giá cho một đôi lên đến 32 triệu đồng. Sở dĩ đôi giày này đắt vì thiết kế độc đáo, hầm hố kết hợp với dáng vẻ thể thao. Cũng chính vì điều này mà nhiều sao hạng A, diễn viên Hollywood mang nó trong những buổi họp báo mà đôi giày nhanh chóng được nhiều bạn trẻ đón nhận.</p>
<h2 id="Giày_Balenciaga_nặng_bao_nhiêu_kg" title="Giày Balenciaga nặng bao nhiêu kg"><span id="Giay_Balenciaga_nang_bao_nhieu_kg">Giày Balenciaga nặng bao nhiêu kg</span></h2>
<p>Giày Balenciaga có đến 3 phiên bản khác nhau, mỗi phiên bản đều mang những nét đặc trưng riêng biệt về thiết kế, chất liệu, và giá cả. Nên việc đánh giá chính xác <strong>giày Balenciaga nặng Bao nhiêu Kg</strong> thì chúng ta cần phân tích từng phiên bản một.</p>
<ul>
<li>
<h3 id="Balenciaga_Triple_S_nặng_bao_nhiêu_kg?" title="Balenciaga Triple S nặng bao nhiêu kg?"><span id="Balenciaga_Triple_S_nang_bao_nhieu_kg"><span style="color: #ff6600;">Balenciaga Triple S nặng bao nhiêu kg?</span></span></h3>
</li>
</ul>
<p>Giày Balenciaga Triple S có cân nặng là <strong><span style="color: #ff0000;">1.7 &#8211; 2.05</span> kg</strong> 1 đôi. Sở dĩ mức cân nặng này không cố định được vì còn phụ thuộc vào size của giày. Size càng lớn thì cân nặng càng cao, ở những size lớn nhất có thể lên đến 3 kg, đây là đôi giày nặng nhất trong 3 phiên bản Sneaker của Balenciaga.</p>
<div id="attachment_2644" style="width: 802px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2644" loading="lazy" class="wp-image-2644" src="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg.jpg" alt="giày balenciaga nặng bao nhiêu kg, balenciaga nặng bao nhiêu kg, balenciaga triple s nặng bao nhiêu kg, giày balenciaga nặng bao nhiêu, balenciaga track nặng bao nhiêu, balenciaga real nặng bao nhiêu, đôi balenciaga nặng bao nhiêu kg, balenciaga real nặng bao nhiêu kg, balenciaga rep nặng bao nhiêu, đôi balenciaga nặng bao nhiêu" width="792" height="1058" /><p id="caption-attachment-2644" class="wp-caption-text">Đôi Balenciaga Triple s là có cân nặng lớn nhất</p></div>
<p>Lí giải việc <a title="giày Balenciaga Triple S" href="https://giayrep.vn/giay-balenciaga-triple-s/" target="_blank" rel="noopener noreferrer">giày Balenciaga Triple S</a> có cân nặng cao đến như vậy là vì Triple S mang một dáng vẻ khổng lồ, và chất liệu cao cấp như mesh, da dê, da trơn, lưới&#8230; Có lẽ cũng chính vì sự hầm hố ấy mà những sao hạng A chọn mang nó như muốn nổi bật và gây sự chú ý giữa đám đông.</p>
<p>Với thiết kế đẹp bắt mắt nhưng không phải không có nhược điểm, chính vì sự cồng kềnh và hầm hố về cân nặng nên những bạn sử dụng Balenciaga Triple S để đi chơi xa, cần đi bộ nhiều nên cân nhắc lựa chọn để có được sự thoải mái cho đôi chân.</p>
<ul>
<li>
<h3 id="Balenciaga_Track_nặng_bao_nhiêu?" title="Balenciaga Track nặng bao nhiêu?"><span id="Balenciaga_Track_nang_bao_nhieu"><span style="color: #ff6600;">Balenciaga Track nặng bao nhiêu?</span></span></h3>
</li>
</ul>
<p>Giày Balenciaga Track có cân nặng khoảng <strong><span style="color: #ff0000;">2</span> kg</strong>, đây là mức cân nặng không quá cao. Khi mang đôi giày này cũng không gặp khó khăn như Triple S nên có thể sử dụng để đi bộ dài mà không lo mỏi chân.</p>
<div id="attachment_2646" style="width: 802px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2646" loading="lazy" class="wp-image-2646 size-full" src="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-3.jpg" alt="giày balenciaga nặng bao nhiêu kg, balenciaga nặng bao nhiêu kg, balenciaga triple s nặng bao nhiêu kg, giày balenciaga nặng bao nhiêu, balenciaga track nặng bao nhiêu, balenciaga real nặng bao nhiêu, đôi balenciaga nặng bao nhiêu kg, balenciaga real nặng bao nhiêu kg, balenciaga rep nặng bao nhiêu, đôi balenciaga nặng bao nhiêu" width="792" height="594" srcset="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-3.jpg 792w, https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-3-768x576.jpg 768w" sizes="(max-width: 792px) 100vw, 792px" /><p id="caption-attachment-2646" class="wp-caption-text">Balenciaga Track là đôi phù hợp nhất, không quá nặng cũng không nhẹ quá</p></div>
<p>Đôi Balenciaga Track có thiết kế khá &#8220;Dị&#8221; và tinh tế với đặc điểm là phần thân giày được chắp nối bằng nhiều lớp da, vải chồng chéo lên nhau, nhìn rất rối mắt nhưng vẫn hợp lí vì nằm trong sự tính toán của nhà thiết kế. Chính cái &#8220;dị&#8221; đó lại tạo nên một tổng thể đầy sáng tạo và cuốn hút.</p>
<ul>
<li>
<h3 id="Balenciaga_Trainer_nặng_bao_nhiêu_kg" title="Balenciaga Trainer nặng bao nhiêu kg"><span id="Balenciaga_Trainer_nang_bao_nhieu_kg"><span style="color: #ff6600;">Balenciaga Trainer nặng bao nhiêu kg?</span></span></h3>
</li>
</ul>
<p>Giày Balenciaga Trainer có cân nặng chỉ <strong><span style="color: #ff0000;">250</span> g</strong>. Đây là đôi giày nhẹ nhất trong 3 phiên bản Sneaker của Balenciaga. Nên việc mang đôi này đi sẽ rất thoải mái và nhẹ nhàng.</p>
<div id="attachment_2647" style="width: 806px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2647" loading="lazy" class="wp-image-2647 size-full" src="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-4.jpg" alt="giày balenciaga nặng bao nhiêu kg, balenciaga nặng bao nhiêu kg, balenciaga triple s nặng bao nhiêu kg, giày balenciaga nặng bao nhiêu, balenciaga track nặng bao nhiêu, balenciaga real nặng bao nhiêu, đôi balenciaga nặng bao nhiêu kg, balenciaga real nặng bao nhiêu kg, balenciaga rep nặng bao nhiêu, đôi balenciaga nặng bao nhiêu" width="796" height="529" srcset="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-4.jpg 796w, https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-4-768x510.jpg 768w" sizes="(max-width: 796px) 100vw, 796px" /><p id="caption-attachment-2647" class="wp-caption-text">Balenciaga Trainer là đôi nhẹ nhất bởi thiết kế tối giản</p></div>
<p>Với thiết kế có phần tối giản nhưng vẫn sở hữu được phong cách mạnh mẽ, sự đan xen tinh tế giữa nghệ thuật và thời trang. Phần thân trên của giày Balenciaga Trainer sử dụng chất liệu Snit Sock, phần đế được thiết kế để trợ lực giúp mỗi bước đi trở nên nhẹ nhàng hơn.</p>
<h2 id="1_đôi_giày_nặng_bao_nhiêu_kg" title="1 đôi giày nặng bao nhiêu kg"><span id="1_doi_giay_nang_bao_nhieu_kg">1 đôi giày nặng bao nhiêu kg</span></h2>
<p>Cân nặng của 1 đôi giày thông thường dao động trong khoảng từ <span style="color: #ff0000;">200</span>g &#8211; <span style="color: #ff0000;">311,84</span>g là có thể vừa cho tất cả các kích thước chân. Với một số dòng giày siêu nhẹ thì cần nặng sẽ từ <span style="color: #ff0000;">200</span>g trở xuống, những đôi giày nhẹ này hầu hết thích hợp để chạy thể thao.Từ số liệu này sẽ giúp bạn dễ hình dung và so sánh với cân nặng của giày Balenciaga.</p>
<h2 id="Mua_giày_balenciaga_ở_đâu_uy_tín_giá_rẻ" title="Mua giày balenciaga ở đâu uy tín giá rẻ"><span id="Mua_giay_balenciaga_o_dau_uy_tin_gia_re">Mua giày balenciaga ở đâu uy tín giá rẻ</span></h2>
<p>Để có thể chọn được một địa chỉ <a title="mua giày balenciaga giá rẻ" href="https://giayrep.vn/giay-balenciaga-rep/" target="_blank" rel="noopener noreferrer">mua giày balenciaga giá rẻ</a>, chất lượng tốt trên thị trường là một điều khó khăn với những bạn chưa có kinh nghiệm. Hiện nay có nhiều cơ sở cung cấp giày kém chất lượng nhưng lại bán với giá rất cao, nên hãy lưu ý giá cả chưa chắc đã đi đôi với chất lượng và thương hiệu. Tại <a title="Shop Giày Rep" href="https://giayrep.vn/" target="_blank" rel="noopener noreferrer">Shop Giày Rep</a> luôn đặt tiêu chí chất liệu sản phẩm tốt nhất nhưng giá thành lại tối ưu nhất lên hàng đầu.</p>
<p>Hiện tại Shop cung cấp nhiều mẫu mã, phối màu, size giày Balenciaga cho bạn lựa chọn . Giày mua tại shop sẽ được hỗ trợ phí vận chuyển trên toàn quốc, kiểm tra hàng trước thanh toán và nhiều ưu đãi khác.</p>
<div id="attachment_2649" style="width: 802px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2649" loading="lazy" class="wp-image-2649" src="https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-6.jpg" alt="giày balenciaga nặng bao nhiêu kg, balenciaga nặng bao nhiêu kg, balenciaga triple s nặng bao nhiêu kg, giày balenciaga nặng bao nhiêu, balenciaga track nặng bao nhiêu, balenciaga real nặng bao nhiêu, đôi balenciaga nặng bao nhiêu kg, balenciaga real nặng bao nhiêu kg, balenciaga rep nặng bao nhiêu, đôi balenciaga nặng bao nhiêu" width="792" height="920" /><p id="caption-attachment-2649" class="wp-caption-text">Shop bán giày Balenciaga Rep 1:1 chất lượng hàng đầu</p></div>
<p>Hy vọng qua bài viết <strong>Đôi Giày balenciaga nặng bao nhiêu kg</strong> đã giúp bạn trả lời được thắc mắc, từ đó chọn cho mình một đôi giày Balenciaga phù hợp. Nếu có ý kiến đóng góp nào khác, các bạn hãy inbox trực tiếp cho shop để được phản hồi trong thời gian sớm nhất nhé.</p>
<p>Xem thêm:</p>
<ol>
<li><a title="Khái niệm giày replica là gì" href="https://giayrep.vn/khai-niem-giay-replica-la-gi/" target="_blank" rel="noopener noreferrer" aria-current="page">Khái niệm giày replica là gì? Có nên mua hay không?</a></li>
<li><a title="Cách phối đồ với giày adidas" href="https://giayrep.vn/cach-phoi-do-voi-giay-adidas/" target="_blank" rel="noopener noreferrer">Cách phối đồ với giày adidas cho nam và nữ đẹp ngất ngây</a></li>
<li><a title="Cách buộc dây giày yeezy 700 siêu chất" href="https://giayrep.vn/cach-buoc-day-giay-yeezy-700/" target="_blank" rel="noopener noreferrer">Cách buộc dây giày yeezy 700 siêu chất</a></li>
<li><a title="phối đồ với giày Adidas Yeezy 700 c" href="https://giayrep.vn/cach-phoi-do-voi-giay-adidas-yeezy-700/" target="_blank" rel="noopener noreferrer">Cách phối đồ với giày Adidas Yeezy 700 cho nam và nữ đẹp nhất</a>
<div id="recent-posts-4" class="sidebar-widget widget_recent_entries"></div>
</li>
</ol>
					</div>', N'https://giayrep.vn/wp-content/uploads/2020/06/giay-balenciaga-nang-bao-nhieu-kg-2.jpg', NULL, NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (5, N'Cách phối đồ với giày Adidas Yeezy 700 cho nam và nữ đẹp nhất', N'<div class="entry-content">
			<p>Được biết đến là đôi giày được nhiều bạn trẻ trên thế giới yêu thích bởi nét cá tính riêng biệt, Giày Adidas Yeezy vẫn đang từng ngày tung ra những bản thiết kế để bắt kịp xu hướng thời trang. Nhưng khi đã sở hữu được đôi Sneaker mà mình luôn mong ước thì việc đi nó ra sao, phối đồ như thế nào để đạt được hiệu quả nhất lại là câu hỏi của nhiều bạn. Hãy cùng theo dõi bài viết hướng dẫn <a title="Cách phối đồ với giày Adidas Yeezy 700" href="https://giayrep.vn/cach-phoi-do-voi-giay-adidas-yeezy-700/"><strong>cách phối đồ với giày Adidas Yeezy 700</strong></a> cho nam và nữ đẹp nhất mà Shop Giày Rep chia sẻ sau đây nhé!</p>
<h2><img loading="lazy" class="aligncenter wp-image-2041" title="Cách phối đồ với giày Adidas Yeezy 700 cho nam và nữ" src="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-3-240x300.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="750" srcset="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-3-240x300.jpg 240w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-3.jpg 491w" sizes="(max-width: 600px) 100vw, 600px" /></h2>
<p style="text-align: center;"><em>Cách phối đồ với giày Adidas Yeezy 700 cho nam và nữ</em></p>
<div id="toc_container" class="no_bullets"><p class="toc_title">Mục Lục</p><ul class="toc_list"><li><a href="#Tai_sao_Yeezy_700_duoc_nhieu_ban_tre_lua_chon"><span class="toc_number toc_depth_1">1</span> Tại sao Yeezy 700 được nhiều bạn trẻ lựa chọn</a><ul><li><a href="#Thiet_ke_de_giay"><span class="toc_number toc_depth_2">1.1</span> Thiết kế đế giày</a></li><li><a href="#Thiet_ke_ngoai_hinh_dac_trung"><span class="toc_number toc_depth_2">1.2</span> Thiết kế ngoại hình đặc trưng</a></li><li><a href="#Chat_lieu_tao_nen_doi_giay"><span class="toc_number toc_depth_2">1.3</span> Chất liệu tạo nên đôi giày</a></li></ul></li><li><a href="#Diem_yeu_cua_Yeezy_700"><span class="toc_number toc_depth_1">2</span> Điểm yếu của Yeezy 700</a><ul><li><a href="#Kho_phoi_do"><span class="toc_number toc_depth_2">2.1</span> Khó phối đồ</a></li><li><a href="#Nhung_phoi_mau_gioi_han"><span class="toc_number toc_depth_2">2.2</span> Những phối màu giới hạn</a></li><li><a href="#Muc_gia_cao"><span class="toc_number toc_depth_2">2.3</span> Mức giá cao</a></li></ul></li><li><a href="#Cach_phoi_do_voi_giay_Adidas_Yeezy_700"><span class="toc_number toc_depth_1">3</span> Cách phối đồ với giày Adidas Yeezy 700</a><ul><li><a href="#Cach_phoi_do_voi_Yeezy_700_cho_Nam"><span class="toc_number toc_depth_2">3.1</span> Cách phối đồ với Yeezy 700 cho Nam</a></li><li><a href="#Cach_phoi_do_voi_Yeezy_700_cho_Nu"><span class="toc_number toc_depth_2">3.2</span> Cách phối đồ với Yeezy 700 cho Nữ</a></li></ul></li><li><a href="#Di_giay_yeezy_700_mac_voi_quan_gi_dep_nhat"><span class="toc_number toc_depth_1">4</span> Đi giày yeezy 700 mặc với quần gì đẹp nhất</a><ul><li><a href="#Giay_yeezy_700_mac_voi_quan_ran_ri"><span class="toc_number toc_depth_2">4.1</span> Giày yeezy 700 mặc với quần rằn ri</a></li><li><a href="#Di_giay_yeezy_700_mac_voi_quan_the_thao_bo_ong"><span class="toc_number toc_depth_2">4.2</span> Đi giày yeezy 700 mặc với quần thể thao bó ống</a></li><li><a href="#Quan_Jean_ket_hop_voi_giay_yeezy_700"><span class="toc_number toc_depth_2">4.3</span> Quần Jean kết hợp với giày yeezy 700</a></li></ul></li></ul></div>
<h2 id="Tại_sao_Yeezy_700_được_nhiều_bạn_trẻ_lựa_chọn" title="Tại sao Yeezy 700 được nhiều bạn trẻ lựa chọn"><span id="Tai_sao_Yeezy_700_duoc_nhieu_ban_tre_lua_chon">Tại sao Yeezy 700 được nhiều bạn trẻ lựa chọn</span></h2>
<p><a title="Giày Adidas Yeezy" href="https://giayrep.vn/giay-adidas-yeezy/" target="_blank" rel="noopener noreferrer">Giày Adidas Yeezy</a> được thiết kế bởi Kayne West, ông từng là một rapper nổi tiếng. Trước khi về làm việc cho hãng giày Adidas ông cũng từng là nhà thiết kế cho Nike. Để Yeezy 700 được nhiều bạn lựa chọn mua trong khi giá thành có nó lại rất cao là bởi vì những yếu tố sau:</p>
<h3 style="padding-left: 40px;"><span id="Thiet_ke_de_giay"><span style="color: #ff6600;">Thiết kế đế giày</span></span></h3>
<p>Mang lại cảm giác thoải mái khi đi, tạo được độ đàn hồi, đệm khi chạy nhảy là điểm nổi bật của công nghệ Boost. Sở dĩ đôi giày này có giá cao một phần là do Yeezy 700 được trang bị công nghệ này. Thiết kế đế Boost cũng là một đặc điểm mà Yeezy 700 Replica không thể sao chép được.</p>
<h3 style="padding-left: 40px;"><span id="Thiet_ke_ngoai_hinh_dac_trung"><span style="color: #ff6600;">Thiết kế ngoại hình đặc trưng</span></span></h3>
<p>Lấy cảm hứng từ ván lướt sóng, các phiên bản của Yeezy 700 luôn mang một hình dáng đặc trưng. Lúc đầu hình dáng này bị đánh giá khá xấu, to, thô và cục mịch. Nhưng một thời gian sau thì các bạn trẻ nhất ra nét đẹp riêng của đôi giày, như một cơn sốt đôi giày trở thành xu hướng (trend) và thiết kế của <a title="Giày Adidas Yeezy 700" href="https://giayrep.vn/yeezy-700/" target="_blank" rel="noopener noreferrer">Giày Adidas Yeezy 700</a> được rất nhiều bạn trẻ ưa thích.</p>
<h3><img loading="lazy" class="aligncenter wp-image-2034" title="Yeezy 700 luôn có thiết kế ngoại hình đặc trưng" src="https://giayrep.vn/wp-content/uploads/2020/04/Adidas-Yeezy-700-3-300x197.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="395" srcset="https://giayrep.vn/wp-content/uploads/2020/04/Adidas-Yeezy-700-3-300x197.jpg 300w, https://giayrep.vn/wp-content/uploads/2020/04/Adidas-Yeezy-700-3-768x505.jpg 768w, https://giayrep.vn/wp-content/uploads/2020/04/Adidas-Yeezy-700-3.jpg 923w" sizes="(max-width: 600px) 100vw, 600px" /></h3>
<p style="text-align: center;"><em>Yeezy 700 luôn có thiết kế ngoại hình đặc trưng</em></p>
<h3 style="padding-left: 40px;"><span id="Chat_lieu_tao_nen_doi_giay"><span style="color: #ff6600;">Chất liệu tạo nên đôi giày</span></span></h3>
<p>Yeezy 700 được tạo nên bởi chất liệu da lộn và cao su cao cấp nên độ bền rất cao. Với chất lượng cao sẽ giúp đôi giày này bền hơn rất nhiều, người sử dụng có thể chạy nhảy mà không lo có vấn đề không đáng có xảy ra. Việc vệ sinh đôi giày cũng trở nên dễ dàng hơn.</p>
<h2 id="Điểm_yếu_của_Yeezy_700" title="Điểm yếu của Yeezy 700"><span id="Diem_yeu_cua_Yeezy_700">Điểm yếu của Yeezy 700</span></h2>
<p>Bên cạnh những điểm mạnh của đôi giày thì mặt tiêu cực của nó cũng là vấn đề rào cản với nhiều người khi không dám lựa chọn đôi giày này.</p>
<h3 style="padding-left: 40px;"><span id="Kho_phoi_do"><span style="color: #ff6600;">Khó phối đồ</span></span></h3>
<p>Được thiết kế theo phong cách chunky khiến <strong>cách phối đồ với giày Yeezy 700</strong> gặp không ít khó khăn bởi thiết kế cục mịch nên rất kén đồ.</p>
<h3 style="padding-left: 40px;"><span id="Nhung_phoi_mau_gioi_han"><span style="color: #ff6600;">Những phối màu giới hạn</span></span></h3>
<p>&#8211; Thường những đôi giày Adidas yeezy 700 được thiết kế theo phiên bản giới hạn, nên sở hữu được một đôi chính hãng là điều khó khăn. Thậm chí khi mẫu mới vừa ra việc xếp hàng từ sớm cũng không đem lại được kết quả khả quan vì quá nhiều người cũng xếp hàng giống bạn.</p>
<h3 style="padding-left: 40px;"><span id="Muc_gia_cao"><span style="color: #ff6600;">Mức giá cao</span></span></h3>
<p>Giày Adidas Yeezy thường được bán với mức giá khá cao. Nên việc chọn mua một đôi giày phù hợp với túi tiền cũng là điều đắn đo của nhiều bạn trẻ. Một đôi Yeezy chính hãng giá thấp nhất cũng đã rơi vào tầm 8.000.000 VNĐ &#8211; 10.000.000 VNĐ. Trong khi đó thì Giày Adidas Yeezy Replica 1:1 chỉ có giá 700.000 VNĐ, vì vậy nên nhiều bạn chọn bản rep thay vì giày chính hãng.</p>
<h2 id="Cách_phối_đồ_với_giày_Adidas_Yeezy_700" title="Cách phối đồ với giày Adidas Yeezy 700"><span id="Cach_phoi_do_voi_giay_Adidas_Yeezy_700">Cách phối đồ với giày Adidas Yeezy 700</span></h2>
<p>Phong cách mặc đồ của nam và nữ khác nhau nên không thể cùng một loại giày mà sử dụng cách phối cho 2 giới tính giống nhau được nên Shop Giày Rep sẽ chia <strong>Cách phối đồ với giày Adidas Yeezy 700</strong> ra 2 nhóm sau.</p>
<h3 id="Cách_phối_đồ_với_Yeezy_700_cho_Nam" style="padding-left: 40px;" title="Cách phối đồ với Yeezy 700 cho Nam"><span id="Cach_phoi_do_voi_Yeezy_700_cho_Nam"><span style="color: #ff6600;">Cách phối đồ với Yeezy 700 cho Nam</span></span></h3>
<p>Cách phối đồ với giày Yeezy 700 cho Nam có thể chọn quần Jean và áo Hoodie. Đây là lựa chọn mà nhiều bạn nam ưa thích bởi phong cách bụi bặm, đường phố. Nếu bạn không thích quần jean thì có thể sử dụng quần thể thao hoặc quần tây để MIX đồ với giày Yeezy 700.</p>
<p><img loading="lazy" class="aligncenter wp-image-2040" title="Yeezy 700 Wave Runner kết hợp với áo Hoodie rất đường phố" src="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-2-280x300.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="644" srcset="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-2-280x300.jpg 280w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-2.jpg 577w" sizes="(max-width: 600px) 100vw, 600px" /></p>
<p style="text-align: center;"><em>Yeezy 700 Wave Runner kết hợp với áo Hoodie rất đường phố</em></p>
<p>Nếu bạn là người ưa thích phong cách bụi bặm đường phố thì tại sao không thử phối đồ với yz700 bằng áo khoác cổ điển và chiếc quần thể thao đơn giản. Chông bạn có vẻ giản dị nhưng vẫn giữ được độ &#8220;chất&#8221; cần thiết nhé!</p>
<p><img loading="lazy" class="aligncenter wp-image-2039" title="Adidas Yeezy 700 mix với quần thể thao và áo khoác bụi bặm" src="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-300x300.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="601" srcset="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-300x300.jpg 300w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-100x100.jpg 100w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-150x150.jpg 150w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700.jpg 622w" sizes="(max-width: 600px) 100vw, 600px" /></p>
<p style="text-align: center;"><em>Adidas Yeezy 700 mix với quần thể thao và áo khoác bụi bặm</em></p>
<p>Áo trong đen kết hợp với quần bò đen rách và <a title="giày Yeezy 700 Inertia" href="https://giayrep.vn/adidas-yeezy-boost-700-inertia/" target="_blank" rel="noopener noreferrer">giày Yeezy 700 Inertia</a> là lựa chọn rất ngầu dành cho các bạn nam, nhưng để tránh case này bị nhàm, bạn nên chọn áo khoác có tông màu sáng. Nó sẽ giúp nổi bật bộ đồ đen và đôi giày lên nhiều đấy.</p>
<div id="attachment_2679" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2679" loading="lazy" class="wp-image-2679" title="Phối đồ với Yeezy 700 Inertia siêu cá tính cho bạn nam " src="https://giayrep.vn/wp-content/uploads/2020/05/phoi-do-voi-yeezy-700-inertia.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="575" srcset="https://giayrep.vn/wp-content/uploads/2020/05/phoi-do-voi-yeezy-700-inertia.jpg 971w, https://giayrep.vn/wp-content/uploads/2020/05/phoi-do-voi-yeezy-700-inertia-768x736.jpg 768w" sizes="(max-width: 600px) 100vw, 600px" /><p id="caption-attachment-2679" class="wp-caption-text">Phối đồ với Yeezy 700 Inertia siêu cá tính cho bạn nam </p></div>
<h3 id="Cách_phối_đồ_với_Yeezy_700_cho_Nữ" style="padding-left: 40px;" title="Cách phối đồ với Yeezy 700 cho Nữ"><span id="Cach_phoi_do_voi_Yeezy_700_cho_Nu"><span style="color: #ff6600;">Cách phối đồ với Yeezy 700 cho Nữ</span></span></h3>
<p>Với các bạn nữ <strong>cách phối đồ với giày Adidas Yeezy 700</strong> có phần đơn giản hơn nhưng vẫn giữ được nét đẹp và sự tinh tế của đôi giày. Việc kết hợp <a title="Yeezy 700 Salt" href="https://giayrep.vn/adidas-yeezy-boost-700-salt/" target="_blank" rel="noopener noreferrer">Yeezy 700 Salt</a> với quần thể thao và áo thun là lựa chọn được nhiều bạn nữ ưa thích. Vừa tiết kiệm được túi tiền lại đại hiệu quả cao.</p>
<p><img loading="lazy" class="aligncenter wp-image-2042" title="Yeezy 700 với quần thể thao và áo thun mang đến nét cá tính mạnh cho các bạn nữ" src="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-4-300x162.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="323" srcset="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-4-300x162.jpg 300w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-4-768x414.jpg 768w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-4.jpg 962w" sizes="(max-width: 600px) 100vw, 600px" /></p>
<p style="text-align: center;"><em>Yeezy 700 với quần thể thao và áo thun mang đến nét cá tính mạnh cho các bạn nữ</em></p>
<p><img loading="lazy" class="aligncenter wp-image-2043" title="Kết hợp với quần bó và áo khoác len" src="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-5-237x300.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="761" srcset="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-5-237x300.jpg 237w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-5.jpg 365w" sizes="(max-width: 600px) 100vw, 600px" /></p>
<p style="text-align: center;"><em>Kết hợp với quần bó và áo khoác len</em></p>
<p><img loading="lazy" class="aligncenter wp-image-2044" title="Yeezy 700 Static và bộ Croptop phong cách" src="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-6-300x300.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="600" srcset="https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-6-300x300.jpg 300w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-6-100x100.jpg 100w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-6-150x150.jpg 150w, https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700-6.jpg 614w" sizes="(max-width: 600px) 100vw, 600px" /></p>
<p style="text-align: center;"><em>Yeezy 700 Static và bộ Croptop phong cách</em></p>
<p><a title="Giày Yeezy 700 Static" href="https://giayrep.vn/adidas-yeezy-700-v2-static/" target="_blank" rel="noopener noreferrer">Giày Yeezy 700 Static</a> với tông màu trắng luôn giúp bạn phối đồ nhất, bạn có thể kết hợp với bộ đồ croptop mang đến một nét cá tính, trẻ trung, hẳn bạn sẽ nổi bật và được nhiều người chú ý lắm nếu như diện bộ đồ này.</p>
<h2 id="Đi_giày_yeezy_700_mặc_với_quần_gì_đẹp_nhất" title="Đi giày yeezy 700 mặc với quần gì đẹp nhất"><span id="Di_giay_yeezy_700_mac_voi_quan_gi_dep_nhat">Đi giày yeezy 700 mặc với quần gì đẹp nhất</span></h2>
<p>Việc làm các bạn trẻ đau đầu còn nằm ở vấn đề đi giày yeezy 700 mặc với quần gì đẹp nhất. Trong bài viết này Shop Giày Rep xin gợi ý giúp bạn một số mẫu quần để bạn có thể dễ dàng mix đồ một cách chất nhất.</p>
<h3 id="Giày_yeezy_700_mặc_với_quần_rằn_ri" style="padding-left: 40px;" title="Giày yeezy 700 mặc với quần rằn ri"><span id="Giay_yeezy_700_mac_voi_quan_ran_ri"><span style="color: #ff6600;">Giày yeezy 700 mặc với quần rằn ri</span></span></h3>
<p>Việc phối đồ với <a title="giày yeezy 700 Mauve" href="https://giayrep.vn/adidas-yeezy-700-mauve-rep-1-1/" target="_blank" rel="noopener noreferrer">giày yeezy 700 Mauve</a> và quần rằn ri cho bạn nam có được nét cá tính mạnh, năng động vốn có. Nếu bạn đang muốn phối đồ để cùng bạn bè đến những nơi xa, đi chơi, phượt mà không sợ vết bẩn thì lựa chọn này hoàn toàn hợp lí.</p>
<div id="attachment_2224" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2224" loading="lazy" class="wp-image-2224" title="Giày Yeezy 700 Mauve mặc với quần rằn ri mạnh mẽ" src="https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="337" srcset="https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat.jpg 1276w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-250x140.jpg 250w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-350x196.jpg 350w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-1024x575.jpg 1024w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-768x431.jpg 768w" sizes="(max-width: 600px) 100vw, 600px" /><p id="caption-attachment-2224" class="wp-caption-text">Giày Yeezy 700 Mauve mặc với quần rằn ri mạnh mẽ</p></div>
<h3 id="Đi_giày_yeezy_700_mặc_với_quần_thể_thao_bó_ống" style="padding-left: 40px;" title="Đi giày yeezy 700 mặc với quần thể thao bó ống"><span id="Di_giay_yeezy_700_mac_voi_quan_the_thao_bo_ong"><span style="color: #ff6600;">Đi giày yeezy 700 mặc với quần thể thao bó ống</span></span></h3>
<p>Bạn có thể chọn cách phối <a title="Giày Yeezy 700 Geode" href="https://giayrep.vn/adidas-yeezy-boost-700-v2-geode-replica-1-1/" target="_blank" rel="noopener noreferrer">Giày Yeezy 700 Geode</a> với quần thể thao tiện dụng và năng động. Bạn đừng sợ rằng mặc quần thể thao sẽ &#8220;làm hỏng&#8221; vẻ đẹp của đôi giày bạn đang đi. Bạn sẽ khá bất ngờ đấy, nó sẽ đem lại vẻ ngoài tươi mới khi bạn chọn quần thể thao để phối với giày yeezy 700 đấy.</p>
<div id="attachment_2226" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2226" loading="lazy" class="wp-image-2226" title="Đi giày Adidas Yeezy Boost 700 V2 Geode mặc với quần thể thao bó ống" src="https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-3.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="745" srcset="https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-3.jpg 746w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-3-250x310.jpg 250w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-3-350x434.jpg 350w" sizes="(max-width: 600px) 100vw, 600px" /><p id="caption-attachment-2226" class="wp-caption-text">Đi giày Adidas Yeezy Boost 700 V2 Geode mặc với quần thể thao bó ống</p></div>
<div id="attachment_2225" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2225" loading="lazy" class="wp-image-2225" title="Đi giày Adidas Yeezy Boost 700 V2 Geode mặc với quần thể thao bó ống ảnh 2" src="https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-2.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="899" srcset="https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-2.jpg 618w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-2-250x375.jpg 250w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-2-350x524.jpg 350w" sizes="(max-width: 600px) 100vw, 600px" /><p id="caption-attachment-2225" class="wp-caption-text">Đi giày Adidas Yeezy Boost 700 V2 Geode mặc với quần thể thao bó ống ảnh 2</p></div>
<h3 id="Quần_Jean_kết_hợp_với_giày_yeezy_700" style="padding-left: 40px;" title="Quần Jean kết hợp với giày yeezy 700"><span id="Quan_Jean_ket_hop_voi_giay_yeezy_700"><span style="color: #ff6600;">Quần Jean kết hợp với giày yeezy 700</span></span></h3>
<p>Việc phối với quần Jean cũng được nhiều bạn trẻ lựa chọn, cũng dễ hiểu khi quần Jean được giới trẻ ở Việt Nam đón nhận và chưa từng lỗi mốt trong nhiều năm trở lại đây. Nếu bạn còn đang băn khoăn không biết cách mix sao cho hợp thì có thể tham khảo cách mix này nhé.</p>
<div id="attachment_2227" style="width: 610px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2227" loading="lazy" class="wp-image-2227" title="Quần Jean kết hợp với giày Adidas Yeezy Boost 700 Magnet" src="https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-4.jpg" alt="cách phối đồ với yeezy 700 nam, cách phối đồ với yeezy 700 nữ, cách phối đồ với yeezy 700, cách phối đồ cho yeezy 700, cách phối đồ với giày yeezy 700, cách phối đồ với giày yeezy 700 nam, cách phối đồ với giày yeezy 700 nữ, cách phối đồ với đôi yeezy 700, cách phối đồ với yeezy 700 cho nữ, MIX đồ với Yeezy 700, phối đồ với yz700, phối đồ với yz700 nữ, cách phối đồ với yz700, phối đồ với yz700 nam" width="600" height="434" srcset="https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-4.jpg 790w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-4-250x181.jpg 250w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-4-350x253.jpg 350w, https://giayrep.vn/wp-content/uploads/2020/04/Di-giay-yeezy-700-mac-voi-quan-gi-dep-nhat-4-768x555.jpg 768w" sizes="(max-width: 600px) 100vw, 600px" /><p id="caption-attachment-2227" class="wp-caption-text">Quần Jean kết hợp với giày Adidas Yeezy Boost 700 Magnet</p></div>
<p>Trên đây là bài viết chia sẻ về <strong>Cách phối đồ với giày Adidas Yeezy 700</strong> cho nam và nữ đẹp nhất của <a title="Shop Giày Replica" href="https://giayrep.vn/" target="_blank" rel="noopener noreferrer">Shop Giày Replica</a>. Hy vọng qua bài viết này các bạn có thể lựa chọn được một bộ đồ phù hợp với đôi giày, phong cách của riêng mình. Nếu có đóng góp về bài viết bạn có thể để lại bình luận hoặc inbox trực tiếp qua fanpage.</p>
<p>Xem thêm:</p>
<p><a title="Cách Phối đồ Với Giày Adidas Cho Nam Và Nữ đẹp Ngất Ngây" href="https://giayrep.vn/cach-phoi-do-voi-giay-adidas/" target="_blank" rel="noopener noreferrer">Cách Phối đồ Với Giày Adidas Cho Nam Và Nữ đẹp Ngất Ngây</a></p>
<p><a title="Cách Buộc Dây Giày Yeezy 700 Siêu Chất Nhưng Lại đơn Giản" href="https://giayrep.vn/cach-buoc-day-giay-yeezy-700/" target="_blank" rel="noopener noreferrer">Cách Buộc Dây Giày Yeezy 700 Siêu Chất Nhưng Lại đơn Giản</a></p>
<p><a title="Khái niệm giày replica là gì? Có nên mua hay không?" href="https://giayrep.vn/khai-niem-giay-replica-la-gi/" target="_blank" rel="noopener noreferrer">Khái niệm giày replica là gì? Có nên mua hay không?</a></p>
					</div>', N'https://giayrep.vn/wp-content/uploads/2020/04/cach-phoi-do-voi-giay-adidas-yeezy-700.jpg', NULL, NULL)
INSERT [dbo].[News] ([ID], [Title], [Content], [Image], [created_at], [updated_at]) VALUES (6, N'Cách buộc dây giày yeezy 700 siêu chất', N'<div class="entry-content">
			<p>Để mang lại sự mới mẻ, phong cách cá tính bạn có thể chọn cách buộc dây giày khác với cách buộc truyền thống. Người buộc dây giày đẹp là người có sự kiên nhẫn và sáng tạo, không cần thiết phải khéo tay mới làm được. Trong bài viết này Shop Giày Rep xin hướng dẫn bạn <a title="Cách buộc dây giày yeezy 700 siêu chất" href="https://giayrep.vn/cach-buoc-day-giay-yeezy-700"><strong>cách buộc dây giày yeezy 700</strong></a> đẹp nhất mà bạn cần biết.</p>
<div id="attachment_2503" style="width: 382px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2503" loading="lazy" class="wp-image-2503 size-full" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-buoc-day-giay-yeezy-700-3.jpg" alt="buộc dây giày yeezy 700, dây giày yeezy 700, dây giày yeezy 700 static, buộc dây giày yeezy 700 static, xỏ dây giày yeezy 700, cột dây giày yeezy 700, cách buộc dây giày yz700, buộc dây giày yz700, cách buộc dây giày yz700 đẹp, cách buộc dây giày yz700 đơn giản" width="372" height="621" /><p id="caption-attachment-2503" class="wp-caption-text">Cách buộc dây giày yeezy 700 siêu chất, siêu ngầu</p></div>
<div id="toc_container" class="no_bullets"><p class="toc_title">Mục Lục</p><ul class="toc_list"><li><a href="#Mot_vai_thong_tin_huu_ich_ve_day_giay"><span class="toc_number toc_depth_1">1</span> Một vài thông tin hữu ích về dây giày</a><ul><li><a href="#Khai_niem_day_giay"><span class="toc_number toc_depth_2">1.1</span> Khái niệm dây giày</a></li><li><a href="#Chat_lieu"><span class="toc_number toc_depth_2">1.2</span> Chất liệu</a></li></ul></li><li><a href="#Huong_dan_cach_buoc_day_giay_yeezy_700_an_tuong_nhat"><span class="toc_number toc_depth_1">2</span> Hướng dẫn cách buộc dây giày yeezy 700 ấn tượng nhất</a><ul><li><a href="#Buoc_day_giay_Yeezy_700_kieu_sung_trau"><span class="toc_number toc_depth_2">2.1</span> Buộc dây giày Yeezy 700 kiểu sừng trâu</a></li><li><a href="#Buoc_day_giay_Yeezy_700_kieu_dan_cheo"><span class="toc_number toc_depth_2">2.2</span> Buộc dây giày Yeezy 700 kiểu đan chéo</a></li></ul></li></ul></div>
<h2><span id="Mot_vai_thong_tin_huu_ich_ve_day_giay">Một vài thông tin hữu ích về dây giày</span></h2>
<p>Trước khi tìm hiểu về <strong>cách buộc dây giày yeezy 700</strong> chúng ta cùng &#8220;bỏ túi&#8221; những thông tin hữu ích về dây giày sau nhé.</p>
<ul>
<li>
<h3><span id="Khai_niem_day_giay"><span style="color: #ff9900;">Khái niệm dây giày</span></span></h3>
</li>
</ul>
<p>Dây giày là dây được thiết kế để luồn qua các lỗ được thiết kế sẵn trên giày, hài đầu của dây được đính kim loại cứng để giúp người dùng thuận tiện trong quá trình xỏ dây giày. Vì những đôi giày được thiết kế với kích cỡ định sẵn có thể không vừa chân với tất cả mọi người, nên dây giày có công dụng tạo độ thích hợp, bó sát chân, giúp bạn được thoải mái nhất khi đi giày.</p>
<div id="attachment_2504" style="width: 385px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2504" loading="lazy" class="wp-image-2504 size-full" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-buoc-day-giay-yeezy-700-4.jpg" alt="buộc dây giày yeezy 700, dây giày yeezy 700, dây giày yeezy 700 static, buộc dây giày yeezy 700 static, xỏ dây giày yeezy 700, cột dây giày yeezy 700, cách buộc dây giày yz700, buộc dây giày yz700, cách buộc dây giày yz700 đẹp, cách buộc dây giày yz700 đơn giản" width="375" height="622" /><p id="caption-attachment-2504" class="wp-caption-text">Cách buộc dây giày Yz700 đơn giản</p></div>
<ul>
<li>
<h3><span id="Chat_lieu"><span style="color: #ff9900;">Chất liệu</span></span></h3>
</li>
</ul>
<p>Thông thường chất liệu của dây giày là cotton, sợi tổng hợp. Chất liệu này giúp cho dây giày không bị cù khi ma sát qua lại nhiều trong các lỗ giày và có tính bên hơn khi bị dính nước.</p>
<p>Nhiều loại giày thể thao có sử dụng dây giày có độ co giãn giúp việc thắt dây giày trở nên đơn giản hơn, và giúp thoải mái hơn cho người đi.</p>
<h2 id="Hướng_dẫn_cách_buộc_dây_giày_yeezy_700_ấn_tượng_nhất" title="Hướng dẫn cách buộc dây giày yeezy 700 ấn tượng nhất"><span id="Huong_dan_cach_buoc_day_giay_yeezy_700_an_tuong_nhat">Hướng dẫn cách buộc dây giày yeezy 700 ấn tượng nhất</span></h2>
<p>Giày Yeezy 700 có thiết kế bắt mắt theo phong cách dad shoes , ngoài ra độ bền của dòng giày này cũng rất đáng kinh ngạc. Giày Yeezy 700 không có nhiều lỗ xỏ dây cho nên cách buộc buộc dây giày yeezy 700 cũng không có gì quá khó khăn, bạn có thể tham khảo một số cách buộc sau.</p>
<ul>
<li>
<h3 id="Buộc_dây_giày_Yeezy_700_kiểu_sừng_trâu" title="Buộc dây giày Yeezy 700 kiểu sừng trâu"><span id="Buoc_day_giay_Yeezy_700_kieu_sung_trau"><span style="color: #ff9900;">Buộc dây giày Yeezy 700 kiểu sừng trâu</span></span></h3>
</li>
</ul>
<p>Đây là cách buộc mà nhiều bạn trẻ sử dụng, bởi cá tính mà cách buộc này đem lại thực sự ấn tượng. Việc bạn cần làm là xỏ dây giày qua hàng đầu tiên (Từ dưới lên). Tiếp theo luồn dây chéo giống như kiểu Criss Cross, đến mu bàn tay thì dừng. Khi đó bạn hãy xỏ dây qua phần lỗ kế để có khoảng trống. Như vậy là bạn đã có một tác phẩm siêu ngầu rồi.</p>
<div id="attachment_2501" style="width: 921px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2501" loading="lazy" class="wp-image-2501 size-full" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-buoc-day-giay-yeezy-700-1.jpg" alt="buộc dây giày yeezy 700, dây giày yeezy 700, dây giày yeezy 700 static, buộc dây giày yeezy 700 static, xỏ dây giày yeezy 700, cột dây giày yeezy 700, cách buộc dây giày yz700, buộc dây giày yz700, cách buộc dây giày yz700 đẹp, cách buộc dây giày yz700 đơn giản" width="911" height="621" srcset="https://giayrep.vn/wp-content/uploads/2020/06/cach-buoc-day-giay-yeezy-700-1.jpg 911w, https://giayrep.vn/wp-content/uploads/2020/06/cach-buoc-day-giay-yeezy-700-1-768x524.jpg 768w" sizes="(max-width: 911px) 100vw, 911px" /><p id="caption-attachment-2501" class="wp-caption-text">Buộc kiểu sừng trâu cho Yz700 cũng rất bắt mắt phải không các bạn</p></div>
<ul>
<li>
<h3 id="Buộc_dây_giày_Yeezy_700_kiểu_đan_chéo" title="Buộc dây giày Yeezy 700 kiểu đan chéo"><span id="Buoc_day_giay_Yeezy_700_kieu_dan_cheo"><span style="color: #ff9900;">Buộc dây giày Yeezy 700 kiểu đan chéo</span></span></h3>
</li>
</ul>
<p>Đây được cho là cách buộc đơn giản nhất mà ai cũng có thể làm được. Với những bạn sử dụng Yeezy 700 vào mục đích đi chơi thể thao, chạy bộ hay thậm chí leo núi thì kiểu buộc dây giày Yeezy 700 này là lựa chọn rất đáng để thử.</p>
<p>Cách thực hiện:</p>
<p>+ Bước đầu bạn xỏ dây là lỗ nằm hàng đầu tiên, canh cho phần dây nằm ở ngoài giày. Tiếp theo xỏ phần dây giày phía dưới đâm lên trên xen kẽ nhau. Tương tự với những hàng sau đến ô cuối thì dừng lại.<br />
+ Bước tiếp theo xỏ dây bên trái vào lỗ cuối nằm kế và tiếp tục sang bên phải làm y vậy<br />
+ Bước cuối cùng cầm dây giày bên trái vòng qua bên phải tạo bởi lỗ đối diện, và làm tương tự với dây còn lại thôi.</p>
<div id="attachment_2502" style="width: 380px" class="wp-caption aligncenter"><img aria-describedby="caption-attachment-2502" loading="lazy" class="wp-image-2502 size-full" src="https://giayrep.vn/wp-content/uploads/2020/06/cach-buoc-day-giay-yeezy-700-2.jpg" alt="buộc dây giày yeezy 700, dây giày yeezy 700, dây giày yeezy 700 static, buộc dây giày yeezy 700 static, xỏ dây giày yeezy 700, cột dây giày yeezy 700, cách buộc dây giày yz700, buộc dây giày yz700, cách buộc dây giày yz700 đẹp, cách buộc dây giày yz700 đơn giản" width="370" height="622" /><p id="caption-attachment-2502" class="wp-caption-text">Đôi giày Yeezy 700 Static được buộc kiểu đan chéo nhìn rất bắt mắt</p></div>
<p>Nếu 2 cách trên làm bạn bối rối và không thể thực hiện được thì đừng lo, bạn có thể tham khảo qua video sau đây để có được cái nhìn chi tiết và dễ hình dung về cách buộc dây giày Yeezy 700.</p>
<p>Trong trường hợp bạn vẫn chưa sở hữu được một đôi giày Yeezy 700 để thử những cách buộc dây giày mới lạ thì có thể truy cập <a title="Giày Yeezy 700" href="https://giayrep.vn/yeezy-700/" target="_blank" rel="noopener noreferrer">tại đây</a> để lựa chọn cho mình một đôi phù hợp, hoặc nhắn tin cho shop để được tư vấn!</p>
<p><div class="rll-youtube-player" data-src="https://www.youtube.com/embed/T9e6-MpBsOw" data-id="T9e6-MpBsOw" data-query=""></div><noscript><iframe src="https://www.youtube.com/embed/T9e6-MpBsOw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"><span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span></iframe></noscript></p>
<p><div class="rll-youtube-player" data-src="https://www.youtube.com/embed/xAL7FUpJzkU" data-id="xAL7FUpJzkU" data-query=""></div><noscript><iframe src="https://www.youtube.com/embed/xAL7FUpJzkU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></noscript></p>
<p>Trên đây là bài viết giúp bạn biết được <strong>cách buộc dây giày Yeezy 700</strong> đẹp nhất, hy vọng qua bài viết bạn đã có thể lựa chọn cho mình được một cách buộc ưng ý. Nếu có ý kiến đóng góp hoặc cụ thể cách buộc dây giày nào mới lạ độc đáo hơn bạn đọc có thể để lại bình luận phía dưới, hoặc inbox trực tiếp cho <a title="Shop Giày Rep" href="https://giayrep.vn/" target="_blank" rel="noopener noreferrer">Shop Giày Rep</a> nhé.</p>
<p>Xem thêm:</p>
<ol>
<li style="list-style-type: none;">
<ol>
<li><a title="Khái niệm giày replica là gì" href="https://giayrep.vn/khai-niem-giay-replica-la-gi/" target="_blank" rel="noopener noreferrer" aria-current="page">Khái niệm giày replica là gì? Có nên mua hay không?</a></li>
<li><a title="Cách phối đồ với giày adidas" href="https://giayrep.vn/cach-phoi-do-voi-giay-adidas/" target="_blank" rel="noopener noreferrer">Cách phối đồ với giày adidas cho nam và nữ đẹp ngất ngây</a></li>
<li><a title="phối đồ với giày Adidas Yeezy 700 c" href="https://giayrep.vn/cach-phoi-do-voi-giay-adidas-yeezy-700/" target="_blank" rel="noopener noreferrer">Cách phối đồ với giày Adidas Yeezy 700 cho nam và nữ đẹp nhất</a></li>
<li><a href="https://giayrep.vn/giay-balenciaga-nang-bao-nhieu-kg/">Đôi giày balenciaga nặng bao nhiêu kg? Bật mí cân nặng chính xác</a></li>
</ol>
</li>
</ol>
					</div>', N'https://giayrep.vn/wp-content/uploads/2020/06/cach-buoc-day-giay-yeezy-700-2.jpg', NULL, NULL)
SET IDENTITY_INSERT [dbo].[News] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (1, N'AIR TAILWIND 79 SE BLACK', 1, N'Giày chất lượng cao', 1300000.0000, 1040000.0000, N'https://bizweb.dktcdn.net/thumb/medium/100/336/177/products/6-dad445b8-9d65-4381-a564-02429370df11.jpg?v=1616832056713', N'1 đổi 1', CAST(N'2021-04-01T10:07:16.700' AS DateTime), CAST(N'2021-04-01T10:15:12.877' AS DateTime))
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (8, N'AIR FORCE 1 LV8 KSA GS ''3D GLASSES''', 1, N'Giày chất lượng cao', 1600000.0000, 1280000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-6b384707-f3fe-4b0a-8c06-5e0cd7b104d6.jpg?v=1616479119830', N'1 đổi 1', CAST(N'2021-04-01T10:15:50.813' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (9, N'SEAN CLIVER X DUNK LOW SB ''HOLIDAY SPECIAL''', 1, N'Giày chất lượng cao', 2700000.0000, 2160000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/24-bf8ef4b9-f1e3-4b7f-9485-2fb9a7bc091c.jpg?v=1616479016240', N'1 đổi 1', CAST(N'2021-04-01T10:16:29.693' AS DateTime), CAST(N'2021-04-01T10:16:47.697' AS DateTime))
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (10, N'AIR JORDAN 1 RETRO HIGH OG ''VOLT GOLD''', 1, N'Giày chất lượng cao', 2200000.0000, 1760000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/12-b486e5d2-7d09-4947-838e-62d428bb6716.jpg?v=1616478916000', N'1 đổi 1', CAST(N'2021-04-01T10:17:26.093' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (11, N'AIR JORDAN 1 RETRO HIGH ''85 ''VARSITY RED''', 1, N'Giày chất lượng cao', 3500000.0000, 2800000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/27-3e2e1f85-449a-46f8-85ea-72ddd361145e.jpg?v=1611559414000', N'1 đổi 1', CAST(N'2021-04-01T10:17:57.113' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (12, N'STUSSY X AIR FORCE 1 LOW ''FOSSIL''', 1, N'Giày chất lượng cao', 3200000.0000, 2560000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/9-5404bf6a-5b82-4dd4-bb96-40fe73cc8ad0.jpg?v=1616478847267', N'1 đổi 1', CAST(N'2021-04-01T10:18:29.697' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (13, N'AIR JORDAN 1 MID ''CHICAGO''', 1, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/34-3f5ef5bc-7704-44a9-825a-b7557469bcdf.jpg?v=1611559538610', N'1 đổi 1', CAST(N'2021-04-01T10:18:58.817' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (14, N'AIR JORDAN 1 RETRO HIGH OG ''TOP 3''', 1, N'Giày chất lượng cao', 3100000.0000, 2480000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/23-3d7580df-1cf8-410e-9617-b2f741417a99.jpg?v=1611559594283', N'1 đổi 1', CAST(N'2021-04-01T10:20:00.133' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (15, N'WMNS AIR FORCE 1 SHADOW ''PALE IVORY''', 1, N'Giày chất lượng cao', 1900000.0000, 1520000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-d24925a1-82c5-4fa6-a397-149c7050eb41.jpg?v=1610525299000', N'1 đổi 1', CAST(N'2021-04-01T10:20:31.543' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (16, N'AIR JORDAN 1 MID ''OBSIDIAN''', 1, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/8-7584c7a0-0b3a-4ce6-9d18-c323bbfee832.jpg?v=1608195358000', N'1 đổi 1', CAST(N'2021-04-01T10:21:02.710' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (17, N'AIR JORDAN 1 RETRO HIGH OG ''UNC''', 1, N'Giày chất lượng cao', 2500000.0000, 2000000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/68.jpg?v=1606282794633', N'1 đổi 1', CAST(N'2021-04-01T10:21:27.307' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (18, N'AIR JORDAN 1 RETRO HIGH OG GS ''TURBO GREEN''', 1, N'Giày chất lượng cao', 2200000.0000, 1760000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/32.jpg?v=1605765077000', N'1 đổi 1', CAST(N'2021-04-01T10:21:57.413' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (19, N'SUPREME X AIR FORCE 1 LOW', 1, N'Giày chất lượng cao', 1900000.0000, 1520000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6600.jpg?v=1592394665917', N'1 đổi 1', CAST(N'2021-04-01T10:22:38.347' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (20, N'Air Max 720 Green', 1, N'Giày chất lượng cao', 1600000.0000, 800000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2921.jpg?v=1562751355207', N'1 đổi 1', CAST(N'2021-04-01T10:23:12.927' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (21, N'Air Max 97 Black', 1, N'Giày chất lượng cao', 1500000.0000, 750000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/12-80466ec9-118e-4db8-8596-8ac5c12c09c2.jpg?v=1546919912917', N'1 đổi 1', CAST(N'2021-04-01T10:23:47.230' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (22, N'M2K Tekno', 1, N'Giày chất lượng cao', 1500000.0000, 1200000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/7.jpg?v=1586864907913', N'1 đổi 1', CAST(N'2021-04-01T10:24:46.677' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (23, N'FRAGMENT DESIGN X AIR JORDAN 3 RETRO SP ''WHITE''', 1, N'Giày chất lượng cao', 3600000.0000, 2880000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-831fcf72-a027-4602-b7a7-072af82c2dd4.jpg?v=1611560048870', N'1 đổi 1', CAST(N'2021-04-01T10:25:25.477' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (24, N'OFF-WHITE X WMNS AIR JORDAN 4 SP ''SAIL''', 1, N'Giày chất lượng cao', 3900000.0000, 3120000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/9-425d7ca9-7e1a-4551-b659-1718e4b88b15.jpg?v=1611559971000', N'1 đổi 1', CAST(N'2021-04-01T10:26:09.227' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (25, N'AIR FORCE 1 SHADOW ''HYDROGEN BLUE''', 1, N'Giày chất lượng cao', 1900000.0000, 1520000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-1-5d469925-98d1-4cb7-82e8-51e7dbdad278.jpg?v=1610525332037', N'1 đổi 1', CAST(N'2021-04-01T10:26:40.717' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (26, N'DUNK LOW PRO SB ''HEINEKEN''', 1, N'Giày chất lượng cao', 3800000.0000, 3050000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/2-48d0d16d-e578-47ce-85cc-a7be727ee021.jpg?v=1602405505600', N'1 đổi 1', CAST(N'2021-04-01T10:35:58.697' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (27, N'Stan Smith Appears With A New Tri-Color Heel Tab', 2, N'Giày chất lượng cao', 1100000.0000, 880000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9712.jpg?v=1604392720600', N'1 đổi 1', CAST(N'2021-04-01T11:09:35.213' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (28, N'Yeezy Boost 350 V2 Zyon - 1:1', 2, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6694.jpg?v=1592988687157', N'1 đổi 1', CAST(N'2021-04-01T11:10:05.437' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (29, N'Yeezy Boost 350 V2 linen - 1:1', 2, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6606.jpg?v=1592394853213', N'1 đổi 1', CAST(N'2021-04-01T11:10:31.017' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (30, N'YEEZY BOOST 380 MIST REFLECTIVE', 2, N'Giày chất lượng cao', 2300000.0000, 1600000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/16-6f1033e3-1838-40d7-9e86-04a3c0038498.jpg?v=1586864978850', N'1 đổi 1', CAST(N'2021-04-01T11:11:04.230' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (31, N'YEEZY BOOST 700 ANALOG - REP 1:1', 2, N'Giày chất lượng cao', 3300000.0000, 1650000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3580.jpg?v=1565424145477', N'1 đổi 1', CAST(N'2021-04-01T11:11:41.130' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (32, N'PROPHERE "BLACK WHITE" - REP 1:1', 2, N'Giày chất lượng cao', 1100000.0000, 550000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3096.jpg?v=1562840806277', N'1 đổi 1', CAST(N'2021-04-01T11:12:45.317' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (33, N'Stan Smith Hologram', 2, N'Giày chất lượng cao', 1050000.0000, 850000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/87.jpg?v=1547837364243', N'1 đổi 1', CAST(N'2021-04-01T11:13:19.653' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (34, N'YEEZY 350 V2 BOOST ZEBRA', 2, N'Giày chất lượng cao', 1600000.0000, 1200000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/800502-02.jpg?v=1546919919000', N'1 đổi 1', CAST(N'2021-04-01T11:13:59.163' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (35, N'Stan Smith Green', 2, N'Giày chất lượng cao', 1050000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/50003341-2365870303644467-7038016969261449216-o.jpg?v=1547535181000', N'1 đổi 1', CAST(N'2021-04-01T11:15:03.160' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (36, N'Stan Smith White', 2, N'Giày chất lượng cao', 1050000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/22-0794bee4-5e0e-4a3e-a27f-89e14503e197.jpg?v=1547361689000', N'1 đổi 1', CAST(N'2021-04-01T11:15:27.327' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (37, N'Track Mule', 3, N'Giày chất lượng cao', 3500000.0000, 2800000.0000, N'https://balenciaga.dam.kering.com/m/6cbd5d97c23093e7/Medium-653814W3CP39000_F.jpg?v=4', N'1 đổi 1', CAST(N'2021-04-01T11:17:46.653' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (38, N'Track Mule Black', 3, N'Giày chất lượng cao', 3500000.0000, 2800000.0000, N'https://balenciaga.dam.kering.com/m/41fe2859ea05b5df/Medium-653814W3CP31000_F.jpg?v=4', N'1 đổi 1', CAST(N'2021-04-01T11:18:49.347' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (39, N'Track Hike', 3, N'Giày chất lượng cao', 4000000.0000, 3200000.0000, N'https://balenciaga.dam.kering.com/m/5205767185bd2195/Medium-654867W3CP31000_F.jpg?v=4', N'1 đổi 1', CAST(N'2021-04-01T11:19:42.600' AS DateTime), CAST(N'2021-04-01T11:20:15.827' AS DateTime))
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (40, N'Trip S White', 3, N'Giày chất lượng cao', 1900000.0000, 1280000.0000, N'https://balenciaga.dam.kering.com/m/5e158bc21375bce1/Large-536737W2FW19700_F.jpg', N'1 đổi 1', CAST(N'2021-04-01T11:21:16.780' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (41, N'Trip S Black', 3, N'Giày chất lượng cao', 1900000.0000, 1280000.0000, N'https://balenciaga.dam.kering.com/m/2df4728f55afac78/Large-536737W2FA11090_F.jpg', N'1 đổi 1', CAST(N'2021-04-01T11:22:35.223' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (42, N'MEN''S SPEED CLEAR SOLE SNEAKER IN BLACK', 3, N'Giày chất lượng cao', 2000000.0000, 1600000.0000, N'https://balenciaga.dam.kering.com/m/312a3e2ae622d09f/Large-607544W2DBL1000_F.jpg?v=3', N'1 đổi 1', CAST(N'2021-04-01T11:24:01.443' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (48, N'GUCCI X DISNEY 2020', 4, N'Giày chất lượng cao', 2600000.0000, 2080000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6722.jpg?v=1592988116000', N'1 đổi 1', CAST(N'2021-04-01T11:26:57.630' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (49, N'Gucci Tiger', 4, N'Giày chất lượng cao', 2500000.0000, 1500000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2975.jpg?v=1562751236723', N'1 đổi 1', CAST(N'2021-04-01T11:27:36.100' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (50, N'Gucci Rhyton sneaker with mouth print', 4, N'Giày chất lượng cao', 2900000.0000, 1450000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2964.jpg?v=1562751313383', N'1 đổi 1', CAST(N'2021-04-01T11:28:15.067' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (51, N'Gucci Snake', 4, N'Giày chất lượng cao', 2500000.0000, 1500000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2988.jpg?v=1563162475013', N'1 đổi 1', CAST(N'2021-04-01T11:28:39.630' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (52, N'Gucci Bee', 4, N'Giày chất lượng cao', 2500000.0000, 1500000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf2981.jpg?v=1563162535740', N'1 đổi 1', CAST(N'2021-04-01T11:29:02.877' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (53, N'Vans Vault Style 36 Black', 5, N'Giày chất lượng cao', 1200000.0000, 960000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4671.jpg?v=1575438868000', N'1 đổi 1', CAST(N'2021-04-01T11:29:41.490' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (54, N'Vans Era Checkerboard 2020', 5, N'Giày chất lượng cao', 900000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9057.jpg?v=1598952563603', N'1 đổi 1', CAST(N'2021-04-01T11:30:10.473' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (55, N'VANS VAULT CHECKERBOARD 2020', 5, N'Giày chất lượng cao', 900000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9368.jpg?v=1602243562447', N'1 đổi 1', CAST(N'2021-04-01T11:30:33.477' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (56, N'Vans Old Skool Classic Navy', 5, N'Giày chất lượng cao', 900000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3850.jpg?v=1570871052020', N'1 đổi 1', CAST(N'2021-04-01T11:31:09.717' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (57, N'Vans Vault Style 36 Suede/Canvas', 5, N'Giày chất lượng cao', 900000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3845.jpg?v=1570870922023', N'1 đổi 1', CAST(N'2021-04-01T11:31:34.750' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (58, N'Vans Style 36 Navy', 5, N'Giày chất lượng cao', 950000.0000, 660000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/114.jpg?v=1547967272397', N'1 đổi 1', CAST(N'2021-04-01T11:32:04.073' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (59, N'Vans Style 36 Green', 5, N'Giày chất lượng cao', 950000.0000, 570000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/103.jpg?v=1547967343350', N'1 đổi 1', CAST(N'2021-04-01T11:32:27.727' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (62, N'Vans Style 36 Red', 5, N'Giày chất lượng cao', 950000.0000, 760000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/88-40092ec2-9c6a-4867-8a70-ba056fd33d8f.jpg?v=1547967316000', N'1 đổi 1', CAST(N'2021-04-01T11:33:12.063' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (63, N'Vans Style 36 Orange', 5, N'Giày chất lượng cao', 990000.0000, 594000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf3651-79a63de2-99a6-423b-80f2-b8634848f4f2.jpg?v=1566628717663', N'1 đổi 1', CAST(N'2021-04-01T11:33:45.927' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (64, N'Fear of God x Chuck 70 Hi ''Grey''', 6, N'Giày chất lượng cao', 1400000.0000, 1120000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6679.jpg?v=1592988533490', N'1 đổi 1', CAST(N'2021-04-01T11:34:16.190' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (65, N'1970s White Low', 6, N'Giày chất lượng cao', 1200000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/46425094-2327006747530823-1323242581668134912-o.jpg?v=1546919915250', N'1 đổi 1', CAST(N'2021-04-01T11:34:43.830' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (66, N'1970s Black High', 6, N'Giày chất lượng cao', 1200000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/46420111-2327007577530740-6101184310222520320-o.jpg?v=1546919915000', N'1 đổi 1', CAST(N'2021-04-01T11:35:07.227' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (67, N'Chuck 70s x CDG White High', 6, N'Giày chất lượng cao', 1600000.0000, 920000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/17.jpg?v=1546919922130', N'1 đổi 1', CAST(N'2021-04-01T11:35:33.013' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (68, N'Chuck 70s x CDG White Low', 6, N'Giày chất lượng cao', 1200000.0000, 840000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/12.jpg?v=1546919922000', N'1 đổi 1', CAST(N'2021-04-01T11:35:59.377' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (69, N'1970s Sunflowers', 6, N'Giày chất lượng cao', 1200000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/42669430-2297178037180361-6679355885438894080-o-2297178033847028.jpg?v=1546919922333', N'1 đổi 1', CAST(N'2021-04-01T11:36:20.923' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (72, N'MLB BIG BALL CHUNKY SHINY NEW YORK YANKEES', 7, N'Giày chất lượng cao', 990000.0000, 792000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9239.jpg?v=1611741896000', N'1 đổi 1', CAST(N'2021-04-01T11:37:48.293' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (73, N'MLB NY Cream', 7, N'Giày chất lượng cao', 990000.0000, 792000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4560.jpg?v=1573540617000', N'1 đổi 1', CAST(N'2021-04-01T11:38:13.457' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (74, N'MLB Boston', 7, N'Giày chất lượng cao', 990000.0000, 792000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4570.jpg?v=1573540760000', N'1 đổi 1', CAST(N'2021-04-01T11:38:36.587' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (75, N'MLB Big Ball Chunky a NY', 7, N'Giày chất lượng cao', 990000.0000, 792000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6561.jpg?v=1592120820000', N'1 đổi 1', CAST(N'2021-04-01T11:39:05.280' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (76, N'MLB CHUNKY MONOGRAM LT NEW YORK', 7, N'Giày chất lượng cao', 990000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9689.jpg?v=1604392887000', N'1 đổi 1', CAST(N'2021-04-01T11:39:32.977' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (77, N'MLB BIG BALL CHUNKY MICKEY NY - CREAM', 7, N'Giày chất lượng cao', 990000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf9695.jpg?v=1604393033690', N'1 đổi 1', CAST(N'2021-04-01T11:40:20.263' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (78, N'MLB LA', 7, N'Giày chất lượng cao', 950000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4575.jpg?v=1573540721977', N'1 đổi 1', CAST(N'2021-04-01T11:40:46.100' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (79, N'MLB NY Black', 7, N'Giày chất lượng cao', 950000.0000, 720000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf4586.jpg?v=1573540652670', N'1 đổi 1', CAST(N'2021-04-01T11:41:16.663' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (80, N'New Balance 327 Blue Orange', 10, N'Giày chất lượng cao', 1200000.0000, 960000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6656.jpg?v=1599816561337', N'1 đổi 1', CAST(N'2021-04-01T11:41:46.553' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (81, N'New Balance 327 Pride (2020)', 10, N'Giày chất lượng cao', 1200000.0000, 960000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6674.jpg?v=1592988245783', N'1 đổi 1', CAST(N'2021-04-01T11:42:20.810' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (82, N'New Balance 327 Black (W)', 10, N'Giày chất lượng cao', 1200000.0000, 960000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6661.jpg?v=1592988269917', N'1 đổi 1', CAST(N'2021-04-01T11:42:44.437' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (83, N'New Balance 327 Casablanca Orange', 10, N'Giày chất lượng cao', 1400000.0000, 1120000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6669.jpg?v=1592988336397', N'1 đổi 1', CAST(N'2021-04-01T11:43:22.483' AS DateTime), NULL)
INSERT [dbo].[Product] ([ID], [Name], [ID_Brand], [Description], [Price], [Promotion_Price], [Image], [Warranty], [created_at], [updated_at]) VALUES (84, N'New Balance 327 Casablanca Green', 10, N'Giày chất lượng cao', 1400000.0000, 1120000.0000, N'https://bizweb.dktcdn.net/100/336/177/products/dscf6650.jpg?v=1592988356000', N'1 đổi 1', CAST(N'2021-04-01T11:43:50.810' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[Product_Color] ON 

INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (1, 1, 1, 21, CAST(N'2021-04-02T07:54:00.097' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (2, 3, 11, 20, CAST(N'2021-04-02T07:54:48.653' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (3, 2, 8, 20, CAST(N'2021-04-02T07:55:23.417' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (4, 5, 9, 20, CAST(N'2021-04-02T07:56:03.107' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (5, 4, 10, 20, CAST(N'2021-04-02T07:56:14.077' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (6, 6, 12, 20, CAST(N'2021-04-02T07:56:47.463' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (7, 3, 13, 20, CAST(N'2021-04-02T07:57:35.773' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (8, 5, 14, 20, CAST(N'2021-04-02T07:58:10.507' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (9, 2, 15, 20, CAST(N'2021-04-02T07:58:49.177' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (10, 1, 16, 20, CAST(N'2021-04-02T07:59:10.133' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (11, 5, 17, 20, CAST(N'2021-04-02T07:59:49.603' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (12, 5, 18, 20, CAST(N'2021-04-02T07:59:58.137' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (13, 2, 19, 20, CAST(N'2021-04-02T08:00:25.627' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (14, 5, 20, 20, CAST(N'2021-04-02T08:00:32.750' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (15, 1, 21, 20, CAST(N'2021-04-02T08:00:39.450' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (16, 2, 22, 20, CAST(N'2021-04-02T08:00:52.773' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (17, 2, 23, 20, CAST(N'2021-04-02T08:00:59.650' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (18, 2, 24, 20, CAST(N'2021-04-02T08:01:22.857' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (19, 5, 25, 20, CAST(N'2021-04-02T08:01:34.003' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (20, 5, 26, 20, CAST(N'2021-04-02T08:01:41.467' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (21, 2, 27, 20, CAST(N'2021-04-02T08:01:57.447' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (22, 6, 28, 20, CAST(N'2021-04-02T08:02:12.173' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (23, 4, 29, 20, CAST(N'2021-04-02T08:02:28.337' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (24, 6, 30, 20, CAST(N'2021-04-02T08:02:57.197' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (25, 6, 31, 20, CAST(N'2021-04-02T08:03:30.037' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (26, 1, 32, 20, CAST(N'2021-04-02T08:03:55.613' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (27, 2, 33, 20, CAST(N'2021-04-02T08:04:09.617' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (28, 6, 34, 20, CAST(N'2021-04-02T08:04:26.063' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (29, 5, 35, 20, CAST(N'2021-04-02T08:04:32.960' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (30, 2, 36, 20, CAST(N'2021-04-02T08:04:38.500' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (31, 2, 37, 20, CAST(N'2021-04-02T08:05:06.470' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (32, 1, 38, 20, CAST(N'2021-04-02T08:05:14.860' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (33, 1, 39, 20, CAST(N'2021-04-02T08:05:46.137' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (34, 2, 40, 20, CAST(N'2021-04-02T08:05:51.430' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (35, 1, 41, 20, CAST(N'2021-04-02T08:05:57.817' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (36, 1, 42, 20, CAST(N'2021-04-02T08:06:03.520' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (37, 2, 48, 20, CAST(N'2021-04-02T08:06:26.877' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (38, 2, 49, 20, CAST(N'2021-04-02T08:06:36.660' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (39, 2, 50, 20, CAST(N'2021-04-02T08:06:52.243' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (40, 2, 51, 20, CAST(N'2021-04-02T08:07:02.927' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (41, 2, 52, 20, CAST(N'2021-04-02T08:07:09.290' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (42, 1, 53, 20, CAST(N'2021-04-02T08:07:14.733' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (43, 6, 54, 20, CAST(N'2021-04-02T08:07:27.967' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (44, 6, 55, 20, CAST(N'2021-04-02T08:07:44.320' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (45, 4, 56, 20, CAST(N'2021-04-02T08:07:58.030' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (46, 5, 57, 20, CAST(N'2021-04-02T08:08:16.093' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (47, 4, 58, 20, CAST(N'2021-04-02T08:09:24.930' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (48, 5, 59, 20, CAST(N'2021-04-02T08:09:30.953' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (49, 3, 62, 20, CAST(N'2021-04-02T08:09:40.580' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (50, 7, 63, 20, CAST(N'2021-04-02T08:09:54.987' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (51, 6, 64, 20, CAST(N'2021-04-02T08:10:01.497' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (52, 2, 65, 20, CAST(N'2021-04-02T08:10:08.780' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (53, 1, 66, 20, CAST(N'2021-04-02T08:10:15.193' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (54, 2, 67, 20, CAST(N'2021-04-02T08:10:20.970' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (55, 2, 68, 20, CAST(N'2021-04-02T08:10:30.123' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (56, 7, 69, 20, CAST(N'2021-04-02T08:10:51.587' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (57, 2, 72, 20, CAST(N'2021-04-02T08:11:49.743' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (58, 2, 73, 20, CAST(N'2021-04-02T08:12:06.393' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (59, 2, 74, 20, CAST(N'2021-04-02T08:12:12.633' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (60, 2, 75, 20, CAST(N'2021-04-02T08:12:18.727' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (61, 2, 76, 20, CAST(N'2021-04-02T08:12:25.033' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (62, 2, 77, 20, CAST(N'2021-04-02T08:12:36.177' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (63, 2, 78, 5, CAST(N'2021-04-02T08:12:45.743' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (64, 1, 79, 20, CAST(N'2021-04-02T08:12:51.240' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (65, 7, 80, 5, CAST(N'2021-04-02T08:13:08.823' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (66, 1, 82, 20, CAST(N'2021-04-02T08:13:42.860' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (67, 7, 83, 2, CAST(N'2021-04-02T08:13:57.667' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (68, 5, 84, 20, CAST(N'2021-04-02T08:14:04.403' AS DateTime), NULL, N'39')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (71, 1, 1, 20, CAST(N'2021-04-05T09:59:46.230' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (72, 1, 1, 20, CAST(N'2021-04-05T10:06:26.507' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (73, 2, 8, 20, CAST(N'2021-04-05T10:10:26.797' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (74, 2, 8, 20, CAST(N'2021-04-05T10:10:34.017' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (75, 5, 9, 20, CAST(N'2021-04-05T10:13:07.600' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (76, 5, 9, 20, CAST(N'2021-04-05T10:13:14.667' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (77, 4, 10, 20, CAST(N'2021-04-05T10:13:27.530' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (78, 4, 10, 20, CAST(N'2021-04-05T10:13:35.143' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (79, 3, 11, 20, CAST(N'2021-04-05T10:13:51.653' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (80, 3, 11, 20, CAST(N'2021-04-05T10:13:58.770' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (81, 6, 12, 20, CAST(N'2021-04-05T10:14:19.633' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (82, 6, 12, 20, CAST(N'2021-04-05T10:14:25.823' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (83, 3, 13, 20, CAST(N'2021-04-05T10:15:09.010' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (84, 3, 13, 20, CAST(N'2021-04-05T10:15:15.043' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (85, 5, 14, 20, CAST(N'2021-04-05T10:16:00.943' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (87, 5, 14, 20, CAST(N'2021-04-05T10:16:57.017' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (88, 2, 15, 20, CAST(N'2021-04-05T10:17:17.667' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (89, 2, 15, 20, CAST(N'2021-04-05T10:17:24.047' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (90, 1, 16, 20, CAST(N'2021-04-05T10:17:38.140' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (91, 1, 16, 20, CAST(N'2021-04-05T10:17:44.490' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (92, 5, 17, 20, CAST(N'2021-04-05T10:18:10.163' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (93, 5, 17, 20, CAST(N'2021-04-05T10:18:16.947' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (94, 5, 18, 20, CAST(N'2021-04-05T10:18:39.543' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (95, 5, 18, 20, CAST(N'2021-04-05T10:18:47.850' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (96, 2, 19, 20, CAST(N'2021-04-05T10:19:30.903' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (97, 2, 19, 20, CAST(N'2021-04-05T10:19:38.867' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (98, 5, 20, 20, CAST(N'2021-04-05T10:20:09.743' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (99, 5, 20, 20, CAST(N'2021-04-05T10:20:15.967' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (100, 1, 21, 20, CAST(N'2021-04-05T10:20:30.827' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (101, 1, 21, 20, CAST(N'2021-04-05T10:20:35.307' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (102, 2, 22, 20, CAST(N'2021-04-05T10:21:50.050' AS DateTime), NULL, N'40')
GO
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (103, 2, 22, 20, CAST(N'2021-04-05T10:21:55.110' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (104, 2, 23, 20, CAST(N'2021-04-05T10:22:06.943' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (105, 2, 23, 20, CAST(N'2021-04-05T10:22:11.977' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (106, 2, 24, 20, CAST(N'2021-04-05T10:22:43.417' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (107, 2, 24, 20, CAST(N'2021-04-05T10:22:50.680' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (108, 5, 25, 20, CAST(N'2021-04-05T10:23:13.140' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (109, 5, 25, 20, CAST(N'2021-04-05T10:23:20.260' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (110, 5, 26, 20, CAST(N'2021-04-05T10:23:33.643' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (111, 5, 26, 20, CAST(N'2021-04-05T10:23:40.770' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (112, 2, 27, 20, CAST(N'2021-04-05T10:24:15.023' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (113, 2, 27, 20, CAST(N'2021-04-05T10:24:22.520' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (114, 6, 28, 20, CAST(N'2021-04-05T10:24:35.170' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (115, 6, 28, 20, CAST(N'2021-04-05T10:24:41.800' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (116, 4, 29, 20, CAST(N'2021-04-05T10:25:00.187' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (117, 4, 29, 20, CAST(N'2021-04-05T10:25:11.843' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (118, 6, 30, 20, CAST(N'2021-04-05T10:25:23.287' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (119, 6, 30, 20, CAST(N'2021-04-05T10:25:33.360' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (120, 6, 31, 20, CAST(N'2021-04-05T10:25:41.823' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (121, 6, 31, 20, CAST(N'2021-04-05T10:25:55.140' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (122, 1, 32, 20, CAST(N'2021-04-05T10:26:23.470' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (123, 1, 32, 20, CAST(N'2021-04-05T10:26:30.753' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (124, 2, 33, 20, CAST(N'2021-04-05T10:26:39.923' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (125, 2, 33, 20, CAST(N'2021-04-05T10:26:46.527' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (126, 6, 34, 20, CAST(N'2021-04-05T10:27:04.923' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (127, 6, 34, 20, CAST(N'2021-04-05T10:27:20.270' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (128, 5, 35, 20, CAST(N'2021-04-05T10:27:37.937' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (129, 5, 35, 20, CAST(N'2021-04-05T10:27:46.513' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (130, 2, 36, 20, CAST(N'2021-04-05T10:27:57.017' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (131, 2, 36, 20, CAST(N'2021-04-05T10:28:06.853' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (132, 2, 37, 20, CAST(N'2021-04-05T10:29:18.507' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (133, 2, 37, 20, CAST(N'2021-04-05T10:29:24.637' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (134, 1, 38, 20, CAST(N'2021-04-05T10:29:37.363' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (135, 1, 38, 20, CAST(N'2021-04-05T10:29:44.260' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (136, 1, 39, 20, CAST(N'2021-04-05T10:30:02.883' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (137, 1, 39, 20, CAST(N'2021-04-05T10:30:08.820' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (138, 2, 40, 20, CAST(N'2021-04-05T10:30:24.230' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (139, 2, 40, 20, CAST(N'2021-04-05T10:30:29.357' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (140, 1, 41, 20, CAST(N'2021-04-05T10:30:45.597' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (141, 1, 41, 20, CAST(N'2021-04-05T10:30:50.487' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (142, 1, 42, 20, CAST(N'2021-04-05T10:30:59.303' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (143, 1, 42, 20, CAST(N'2021-04-05T10:31:04.867' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (144, 2, 48, 20, CAST(N'2021-04-05T10:31:34.293' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (145, 2, 48, 20, CAST(N'2021-04-05T10:31:38.980' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (146, 2, 49, 20, CAST(N'2021-04-05T10:31:44.673' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (147, 2, 49, 20, CAST(N'2021-04-05T10:31:49.013' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (148, 2, 50, 20, CAST(N'2021-04-05T10:31:54.467' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (149, 2, 50, 20, CAST(N'2021-04-05T10:31:59.537' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (150, 2, 51, 20, CAST(N'2021-04-05T10:32:05.900' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (151, 2, 51, 20, CAST(N'2021-04-05T10:32:10.790' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (152, 2, 52, 20, CAST(N'2021-04-05T10:32:16.517' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (153, 2, 52, 20, CAST(N'2021-04-05T10:32:22.827' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (154, 1, 53, 20, CAST(N'2021-04-05T10:32:30.600' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (155, 1, 53, 20, CAST(N'2021-04-05T10:32:35.277' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (156, 6, 54, 20, CAST(N'2021-04-05T10:32:55.383' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (157, 6, 54, 20, CAST(N'2021-04-05T10:33:00.167' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (158, 6, 55, 20, CAST(N'2021-04-05T10:33:07.793' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (159, 6, 55, 20, CAST(N'2021-04-05T10:33:19.387' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (160, 4, 56, 20, CAST(N'2021-04-05T10:33:26.917' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (161, 4, 56, 20, CAST(N'2021-04-05T10:33:31.347' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (162, 5, 57, 20, CAST(N'2021-04-05T10:33:39.343' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (163, 5, 57, 20, CAST(N'2021-04-05T10:33:44.037' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (164, 4, 58, 20, CAST(N'2021-04-05T10:33:51.753' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (165, 4, 58, 20, CAST(N'2021-04-05T10:33:56.653' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (166, 5, 59, 20, CAST(N'2021-04-05T10:34:03.997' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (167, 5, 59, 20, CAST(N'2021-04-05T10:34:09.207' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (168, 3, 62, 20, CAST(N'2021-04-05T10:34:38.170' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (169, 3, 62, 20, CAST(N'2021-04-05T10:34:43.143' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (170, 7, 63, 20, CAST(N'2021-04-05T10:34:51.693' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (171, 7, 63, 20, CAST(N'2021-04-05T10:34:56.867' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (172, 6, 64, 20, CAST(N'2021-04-05T10:35:06.083' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (173, 6, 64, 20, CAST(N'2021-04-05T10:35:10.357' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (174, 2, 65, 20, CAST(N'2021-04-05T10:35:19.613' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (175, 2, 65, 20, CAST(N'2021-04-05T10:35:27.830' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (176, 1, 66, 20, CAST(N'2021-04-05T10:35:35.500' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (177, 1, 66, 20, CAST(N'2021-04-05T10:35:40.837' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (178, 2, 67, 20, CAST(N'2021-04-05T10:35:49.947' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (179, 2, 67, 20, CAST(N'2021-04-05T10:35:55.483' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (180, 2, 68, 20, CAST(N'2021-04-05T10:36:08.420' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (181, 2, 68, 20, CAST(N'2021-04-05T10:36:12.713' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (182, 7, 69, 20, CAST(N'2021-04-05T10:36:21.100' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (183, 7, 69, 20, CAST(N'2021-04-05T10:36:25.490' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (184, 2, 72, 20, CAST(N'2021-04-05T10:36:47.637' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (185, 2, 72, 20, CAST(N'2021-04-05T10:36:52.140' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (186, 2, 73, 20, CAST(N'2021-04-05T10:36:57.203' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (187, 2, 73, 20, CAST(N'2021-04-05T10:37:01.243' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (188, 2, 74, 20, CAST(N'2021-04-05T10:37:22.710' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (189, 2, 74, 20, CAST(N'2021-04-05T10:37:26.677' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (190, 2, 75, 20, CAST(N'2021-04-05T10:37:40.350' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (191, 2, 75, 20, CAST(N'2021-04-05T10:37:44.480' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (192, 2, 76, 20, CAST(N'2021-04-05T10:37:48.793' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (193, 2, 76, 20, CAST(N'2021-04-05T10:37:53.913' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (194, 2, 77, 20, CAST(N'2021-04-05T10:38:00.463' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (195, 2, 77, 20, CAST(N'2021-04-05T10:38:05.170' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (196, 2, 78, 20, CAST(N'2021-04-05T10:38:09.377' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (197, 2, 78, 20, CAST(N'2021-04-05T10:38:13.900' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (198, 1, 79, 20, CAST(N'2021-04-05T10:38:23.513' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (199, 1, 79, 20, CAST(N'2021-04-05T10:38:28.207' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (200, 7, 80, 20, CAST(N'2021-04-05T10:38:35.800' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (201, 7, 80, 20, CAST(N'2021-04-05T10:38:40.883' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (202, 1, 82, 20, CAST(N'2021-04-05T10:38:55.357' AS DateTime), NULL, N'40')
GO
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (203, 1, 82, 20, CAST(N'2021-04-05T10:38:59.987' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (204, 7, 83, 20, CAST(N'2021-04-05T10:39:07.000' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (205, 7, 83, 20, CAST(N'2021-04-05T10:39:11.090' AS DateTime), NULL, N'41')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (206, 5, 84, 20, CAST(N'2021-04-05T10:39:17.353' AS DateTime), NULL, N'40')
INSERT [dbo].[Product_Color] ([ID], [ID_Color], [ID_Product], [Quantity], [created_at], [updated_at], [size]) VALUES (207, 5, 84, 20, CAST(N'2021-04-05T10:39:21.037' AS DateTime), NULL, N'41')
SET IDENTITY_INSERT [dbo].[Product_Color] OFF
GO
ALTER TABLE [dbo].[Bill_Detail]  WITH CHECK ADD  CONSTRAINT [fk_Bill_BD] FOREIGN KEY([ID_Bill])
REFERENCES [dbo].[Bills] ([ID])
GO
ALTER TABLE [dbo].[Bill_Detail] CHECK CONSTRAINT [fk_Bill_BD]
GO
ALTER TABLE [dbo].[Bill_Detail]  WITH CHECK ADD  CONSTRAINT [fk_Pro_BD] FOREIGN KEY([ID_Product_Color])
REFERENCES [dbo].[Product_Color] ([ID])
GO
ALTER TABLE [dbo].[Bill_Detail] CHECK CONSTRAINT [fk_Pro_BD]
GO
ALTER TABLE [dbo].[Bills]  WITH CHECK ADD  CONSTRAINT [fk_cus_bill] FOREIGN KEY([ID_Customer])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Bills] CHECK CONSTRAINT [fk_cus_bill]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [fk_Cus_c] FOREIGN KEY([ID_Customer])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [fk_Cus_c]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [fk_Pro_C] FOREIGN KEY([ID_Product_Color])
REFERENCES [dbo].[Product_Color] ([ID])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [fk_Pro_C]
GO
ALTER TABLE [dbo].[Favorites_list]  WITH CHECK ADD  CONSTRAINT [fk_Cus_F] FOREIGN KEY([ID_Customer])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Favorites_list] CHECK CONSTRAINT [fk_Cus_F]
GO
ALTER TABLE [dbo].[Favorites_list]  WITH CHECK ADD  CONSTRAINT [fk_Pro_F] FOREIGN KEY([ID_Product])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Favorites_list] CHECK CONSTRAINT [fk_Pro_F]
GO
ALTER TABLE [dbo].[Imagee]  WITH CHECK ADD  CONSTRAINT [fk_Pro_Ima] FOREIGN KEY([ID_Product])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Imagee] CHECK CONSTRAINT [fk_Pro_Ima]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [fk_br_pro] FOREIGN KEY([ID_Brand])
REFERENCES [dbo].[Brand] ([ID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [fk_br_pro]
GO
ALTER TABLE [dbo].[Product_Color]  WITH CHECK ADD  CONSTRAINT [fk_Co_CoD] FOREIGN KEY([ID_Color])
REFERENCES [dbo].[Colorr] ([ID])
GO
ALTER TABLE [dbo].[Product_Color] CHECK CONSTRAINT [fk_Co_CoD]
GO
ALTER TABLE [dbo].[Product_Color]  WITH CHECK ADD  CONSTRAINT [fk_Pro_CoD] FOREIGN KEY([ID_Product])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Product_Color] CHECK CONSTRAINT [fk_Pro_CoD]
GO
