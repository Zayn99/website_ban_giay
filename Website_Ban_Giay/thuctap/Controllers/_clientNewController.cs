﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using thuctap.Models;
using PagedList; 
namespace thuctap.Controllers
{
    public class _clientNewController : Controller
    {
        // GET: _clientNew
        bangiayDataContext db = new bangiayDataContext();
        public ActionResult Index(int? page)
        {
            var news = from n in db.News
                       orderby n.ID descending
                       select n;
            return View(news.ToPagedList(page?? 1,5));
        }
        public ActionResult NewDetail()
        {
            int key = Convert.ToInt32(Request["key"]);
            var news = from n in db.News
                       where n.ID == key
                       select n;
            return View(news);
        }
    }
}