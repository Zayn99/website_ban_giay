﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using thuctap.Models;
namespace thuctap.Controllers
{
    public class _clientProductController : Controller
    {
        // GET: _clientProduct
        bangiayDataContext db = new bangiayDataContext();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Home()
        {
            ViewBag.product = (from b in db.Products
                               orderby b.ID descending
                               select b).Take(20);
            return View();
        }
    }
}